import express, {Request, Response} from "express"
import { Event } from "./event"

let events: Event[] = []

//Dummy event for testing
events.push(new Event("Dummy event", "Event entry for testing purposes", "24.12.2024", "18:01"))

const router = express.Router()

router.get("/", (_req: Request, res: Response) => {
    res.status(200).send(events)
})

router.get("/:monthNumber", (req: Request, res: Response) => {
    const matchingEvents = events.filter(event => event.date.getMonth() + 1 === Number(req.params.monthNumber))
    res.status(200).send(matchingEvents)
})

router.post("/", (req: Request, res: Response) => {
    const {title, description, date, time} = req.body
    events.push(new Event(title, description, date, time))
    res.sendStatus(201)
})

router.put("/:eventId", (req: Request, res: Response) => {
    const existingEvent = events.find(event => event.id === Number(req.params.eventId))
    if (typeof existingEvent === "undefined") return res.sendStatus(400)
    
    const {title, description, date, time} = req.body
    if (typeof title === "string") existingEvent.title = title
    if (typeof description === "string") existingEvent.description = description
    if (typeof date === "string") existingEvent.setDate(date)
    if (typeof time === "string") existingEvent.setTime(time)

    res.sendStatus(200)
})

router.delete("/:eventId", (req: Request, res: Response) => {
    const existingEvent = events.find(event => event.id === Number(req.params.eventId))
    if (typeof existingEvent === "undefined") return res.sendStatus(400)

    events = events.filter(event => event.id !== Number(req.params.eventId))
    res.sendStatus(200)
})

export default router