let nextEventId = 0

const parseDateString = (str: string): Date => {
    const dateParts = str.split(".")
    const date = new Date()
    date.setFullYear(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0]))
    return date
}

export class Event {
    id: number
    title: string
    description: string
    date: Date

    constructor(title: string,
        description: string,
        date: string, //expected date string format: dd.mm.yyyy
        time?: string) { //expected time string format: hh:mm

        this.id = nextEventId++
        this.title = title
        this.description = description
        
        const dateObj = parseDateString(date)
        this.date = dateObj

        if (typeof time === "undefined" || time === "") {
            time = "00:00"
        }
        this.setTime(time)
    }

    setDate(date: string) {
        const dateObj = parseDateString(date)
        this.date = dateObj
    }

    setTime(time: string) {
        const timeParts = time.split(":") ?? "00:00".split(":")
        this.date.setUTCHours(Number(timeParts[0]))
        this.date.setUTCMinutes(Number(timeParts[1]))
        this.date.setUTCSeconds(0)
        this.date.setUTCMilliseconds(0)
    }

}