import express, {Request, Response} from "express"
import "dotenv/config"

const server = express()
const port = process.env.PORT || 3002

server.get("/", (_req: Request, res: Response) => res.sendStatus(200))
server.get("/version", (_req: Request, res: Response) => res.status(200).send("v1"))
server.use((_req: Request, res: Response) => {res.sendStatus(404)})

server.listen(port, () => console.log("Server listening to", port))