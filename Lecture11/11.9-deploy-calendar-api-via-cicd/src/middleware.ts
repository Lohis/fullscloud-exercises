import {Request, Response} from "express"

export const unknownEndpointResponse = (_req: Request, res: Response) => res.sendStatus(404)

/*
Skipping writing validation middleware for endpoints,
in order to focus on deployment exercises.

Missing validation makes the API prone to crashing and
unexpected behavior.
*/