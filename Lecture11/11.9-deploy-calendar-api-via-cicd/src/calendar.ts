import express, {Request, Response} from "express"
import { unknownEndpointResponse } from "./middleware"
import calendarRouter from "./calendar-router"

const server = express()
server.use(express.json())

server.use("/", calendarRouter)

server.get("/version/version", (_req: Request, res: Response) => res.status(200).send("development v4"))

server.use(unknownEndpointResponse)

export default server