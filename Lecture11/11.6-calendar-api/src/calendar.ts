import express from "express"
import { unknownEndpointResponse } from "./middleware"
import calendarRouter from "./calendar-router"

const server = express()
server.use(express.json())

server.use("/", calendarRouter)
server.use(unknownEndpointResponse)

export default server