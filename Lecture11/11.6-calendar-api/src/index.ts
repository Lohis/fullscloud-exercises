import server from "./calendar"
import "dotenv/config"

const port = process.env.PORT
if (typeof port !== "string" || port === "") console.log("Error: no port environment variable found")
else server.listen(port, () => console.log("Listening to port", port))