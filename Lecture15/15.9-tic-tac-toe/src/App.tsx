import { useState } from "react"
import "./App.css"
import BoardSquare from "./BoardSquare"

const initialBoardState: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0]

function App() {
    const [board, setBoard] = useState(initialBoardState.slice()) //slice method without arguments is used to deep copy the array
    const [currentPlayer, setCurrentPlayer] = useState(1)

    const setSquare = (squareId: number, playerId: number) => {
        const newBoardState = board
        newBoardState[squareId] = playerId
        setBoard(newBoardState)
        setCurrentPlayer(currentPlayer == 1 ? 2 : 1)
    }

    const resetGame = () => {
        setBoard(initialBoardState.slice())
        setCurrentPlayer(1)  
    }

    return (
        <>
            <div>
                Tic-tac-toe
                <button onClick={resetGame}>Reset game</button>
            </div>
            <br />
            <div>
                <BoardSquare id={0} currentPlayer={currentPlayer} owner={board[0]} setSquare={setSquare} />
                <BoardSquare id={1} currentPlayer={currentPlayer} owner={board[1]} setSquare={setSquare} />
                <BoardSquare id={2} currentPlayer={currentPlayer} owner={board[2]} setSquare={setSquare} />
            </div>
            <div>
                <BoardSquare id={3} currentPlayer={currentPlayer} owner={board[3]} setSquare={setSquare} />
                <BoardSquare id={4} currentPlayer={currentPlayer} owner={board[4]} setSquare={setSquare} />
                <BoardSquare id={5} currentPlayer={currentPlayer} owner={board[5]} setSquare={setSquare} />
            </div>
            <div>
                <BoardSquare id={6} currentPlayer={currentPlayer} owner={board[6]} setSquare={setSquare} />
                <BoardSquare id={7} currentPlayer={currentPlayer} owner={board[7]} setSquare={setSquare} />
                <BoardSquare id={8} currentPlayer={currentPlayer} owner={board[8]} setSquare={setSquare} />
            </div>
        </>
    )
}

export default App
