interface BoardSquareProps {
    id: number
    currentPlayer: number
    owner: number
    setSquare: (value1: number, value2: number) => void
}

const BoardSquare = (props : BoardSquareProps) => {
    const { id, currentPlayer, owner, setSquare } = props

    const onBoardSquareClick = () => {
        if (owner == 0) {
            setSquare(id, currentPlayer)
        }
    }

    const setSquareMark = (playerId: number) => {
        if (playerId == 1) return "X"
        if (playerId == 2) return "O"
        return " "
    }

    return (
        <>
            <button className="game-board-btn" onClick={onBoardSquareClick}>
                {setSquareMark(owner)}
            </button>
        </>
    )
}

export default BoardSquare