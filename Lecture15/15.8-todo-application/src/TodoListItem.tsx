import { useState } from "react"
import { TodoItemProps } from "./interfaces"

const TodoListItem = (props: TodoItemProps) => {
    const [taskDoneStatus, setTaskDoneStatus] = useState(props.todoItem.isDone)

    const toggleItemDone = () => {
        setTaskDoneStatus(taskDoneStatus ? false : true)
    }

    const removeItem = () => {
        props.removeItem(props.todoItem.id)
    }

    return (
        <div>
            <input type="checkbox" checked={taskDoneStatus} onChange={toggleItemDone} />
            {props.todoItem.description}
            <button onClick={removeItem}>Remove</button>
        </div>
    )
}

export default TodoListItem