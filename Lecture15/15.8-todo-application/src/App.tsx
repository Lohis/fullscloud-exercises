import { useState, ChangeEvent } from 'react'
import TodoList from './TodoList'
import { TodoItemData } from './interfaces'

const initialTodos: TodoItemData[] = [
    { "id": 0, "description": "Tee lumityöt", "isDone": false },
    { "id": 1, "description": "Tyhjennä tiskikone", "isDone": true },
    { "id": 2, "description": "Pullota kilju", "isDone": false },
    { "id": 3, "description": "Tilaa nuohous", "isDone": false }
]

let nextItemId = initialTodos.length

const App = () => {
    const [inputText, setInputText] = useState("")
    const [todos, setTodos] = useState(initialTodos)
    

    const onInputFieldChange = (event: ChangeEvent<HTMLInputElement>) => {
        setInputText(event.target.value)
    }

    const addTodoItem = (todoDescription: string) => {
        const newTodoItem = new Object as TodoItemData
        newTodoItem.id = nextItemId++
        newTodoItem.description = todoDescription
        newTodoItem.isDone = false

        const newTodos = todos.concat(newTodoItem)
        setTodos(newTodos)
    }

    const removeTodoItem = (itemId: number) => {
        setTodos(todos.filter(item => item.id !== itemId))
    }

    const onAddBtnPress = () => {
        addTodoItem(inputText)
        setInputText("")
    }

    return (
        <div className="App">
            <h1>Todo list</h1>
            <TodoList todos={todos} removeTodoItem={removeTodoItem} />

            <div>
                <input value={inputText} onChange={onInputFieldChange} />
                <button onClick={onAddBtnPress}>
                    Add task
                </button>
            </div>
        </div>
    )
}

export default App
