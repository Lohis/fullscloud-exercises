export interface TodoItemData {
    id: number
    description: string
    isDone: boolean
}

export interface TodoItemProps {
    todoItem: TodoItemData
    removeItem: (value: number) => void
}

export interface TodoListProps {
    todos: TodoItemData[]
    removeTodoItem: (value: number) => void
}
