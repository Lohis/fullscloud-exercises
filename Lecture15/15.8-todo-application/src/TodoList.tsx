import TodoListItem from "./TodoListItem"
import { TodoListProps } from "./interfaces"

const TodoList = (props: TodoListProps) => {
    const { todos, removeTodoItem } = props

    const removeItem = (itemId: number) => {
        removeTodoItem(itemId)
    }

    return (
        todos.map(todoItem => {
            return (
                <TodoListItem todoItem={todoItem} removeItem={removeItem} />
            )
        })
    )

}

export default TodoList