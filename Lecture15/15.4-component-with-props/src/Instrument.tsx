interface Props {
    name: string
    price: number
    image: string
}

const Instrument = (props: Props) => {
    return (
        <div>
            <h2>{props.name}</h2>
            <p>Price: {props.price}</p>
            <img src={props.image} width="250"></img>
        </div>
    )
}

export default Instrument