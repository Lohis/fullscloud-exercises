import Heading from "./Heading"
import Instrument from "./Instrument"
import guitar from "./resources/guitar.png"
import tuba from "./resources/tuba.png"
import violin from "./resources/violin.png"

const App = () => {

  return (
    <>
      <Heading />
      
      <Instrument name={"guitar"}
        price={1200}
        image={guitar} />

      <Instrument name={"tuba"}
        price={4900}
        image={tuba} />

      <Instrument name={"violin"}
        price={2400}
        image={violin} />  
    </>
  )
}

export default App
