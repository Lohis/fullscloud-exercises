import { useState } from 'react'
//import IncrementButton from './IncrementButton'

function App() {
  const [button1Count, setButton1Count] = useState<number>(0)
  const [button2Count, setButton2Count] = useState<number>(0)
  const [button3Count, setButton3Count] = useState<number>(0)

  const incrementCount = (count: number) => count + 1

  return (
    <>
      <div>
        <button onClick={() => setButton1Count(incrementCount)}>
          {button1Count}
        </button>
      </div>

      <div>
        <button onClick={() => setButton2Count(incrementCount)}>
          {button2Count}
        </button>
      </div>

      <div>
        <button onClick={() => setButton3Count(incrementCount)}>
          {button3Count}
        </button>
      </div>

      <p>{button1Count + button2Count + button3Count}</p>
    </>
  )
}

export default App
