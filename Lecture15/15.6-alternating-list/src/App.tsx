
function App() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]
  let isNextNameBold = true  

  return (
    <>
      <h1>Alternating list</h1>
      {namelist.map(name => {
        if (isNextNameBold) {
          isNextNameBold = false
          return <div><b>{name}</b></div>
        }
        else {
          isNextNameBold = true
          return <div><i>{name}</i></div>
        }
      })}
    </>
  )
}

export default App
