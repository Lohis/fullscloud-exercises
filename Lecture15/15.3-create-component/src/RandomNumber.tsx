const generateRandomInt = (min: number, max: number) => {
    const num = Math.floor(Math.random() * (max - min)) + min
    return Math.round(num)
}

const RandomNumber = () => {
    const random = generateRandomInt(1, 101)
    
    return (
        < div className = 'RandomNumber' >
            <p>Your random number is {random}.</p>
        </div >
    )
}

export default RandomNumber