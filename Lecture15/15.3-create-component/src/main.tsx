import React from 'react'
import ReactDOM from 'react-dom/client'
import RandomNumber from './RandomNumber.tsx'
import Heading from './Heading.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Heading />
    <RandomNumber />
  </React.StrictMode>,
)
