import { ChangeEvent, useState } from 'react'

function App() {
  const [userInput, setUserInput] = useState("")
  const [headerText, setHeaderText] = useState("")

  const onInputValueChange = (event: ChangeEvent<HTMLInputElement>) => {
    setUserInput(event.target.value)
  }

  const onSubmitButtonPress = () => {
    setHeaderText(userInput)
  }

  return (
    <div className='App'>
      
      <h1>Your string is: {headerText}</h1>

      <input value={userInput}
        onChange={onInputValueChange} />

      <button onClick={onSubmitButtonPress}>
        Submit
      </button>

    </div>
  )
}

export default App
