class Post {
    author: string
    content: string
    likers: string[]

    constructor(author: string, content: string) {
        this.author = author
        this.content = content
        this.likers = []
    }

    like(user: string) {
        if (!this.likers.includes(user)) this.likers.push(user)
    }

    likes() {
        let str = ""
        const likeAmount = this.likers.length
        if (likeAmount < 1) str = "No one likes this"
        else if (likeAmount === 1) str = `${this.likers[0]} likes this`
        else if (likeAmount === 2) str = `${this.likers[0]} and ${this.likers[1]} like this`
        else if (likeAmount === 3) str = `${this.likers[0]}, ${this.likers[1]} and ${this.likers[2]} like this`
        else str = `${this.likers[0]}, ${this.likers[1]} and ${likeAmount - 2} others like this`

        return str
    }
}

const post = new Post("Roope", "Lorem ipsum")
console.log(post.likes())
post.like("Mummo")
console.log(post.likes())
post.like("Hortensia")
console.log(post.likes())
post.like("Hortensia")
console.log(post.likes())
post.like("Tupu")
console.log(post.likes())
post.like("Hupu")
console.log(post.likes())
post.like("Lupu")
console.log(post.likes())
post.like("Milla")
console.log(post.likes())
