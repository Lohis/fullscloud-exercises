import fs from "fs"

const forecastData = JSON.parse(fs.readFileSync("./data/forecast_data.json", "utf-8"))

forecastData["temperature"] -= 40 //winter is coming

fs.writeFileSync("./data/forecast_data_modified.json", JSON.stringify(forecastData), "utf-8")
