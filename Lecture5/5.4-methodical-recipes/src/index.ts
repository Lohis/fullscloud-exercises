interface Ingredient {
    name: string,
    amount: number
}

interface Recipe {
    name: string,
    ingredients: Array<Ingredient>,
    servings: number,
    setServings: (servings: number) => void
}

const basicBread: Recipe = {
    name: "Basic bread",
    ingredients: [
        {name: "Wheat flour", amount: 500},
        {name: "Tap water", amount: 300},
        {name: "Olive oil", amount: 80},
        {name: "Dry yeast", amount: 11},
        {name: "Salt", amount: 10},
        {name: "Sugar", amount: 25}
    ],
    servings: 10,
    setServings: function(servings) {
        const multiplier = servings / this.servings
        this.ingredients.forEach((ingredient: Ingredient) => ingredient.amount *= multiplier)
        this.servings = servings
    }
}

let dish = `${basicBread.name}, servings: ${basicBread.servings}`
console.log(dish)
basicBread.ingredients.forEach((ingredient) => console.log(ingredient.name, ingredient.amount))
console.log("--------------------")

basicBread.setServings(5)

dish = `${basicBread.name}, servings: ${basicBread.servings}`
console.log(dish)
basicBread.ingredients.forEach((ingredient) => console.log(ingredient.name, ingredient.amount))
console.log("--------------------")

basicBread.setServings(15)

dish = `${basicBread.name}, servings: ${basicBread.servings}`
console.log(dish)
basicBread.ingredients.forEach((ingredient) => console.log(ingredient.name, ingredient.amount))
console.log("--------------------")


