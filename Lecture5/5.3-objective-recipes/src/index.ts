interface Ingredient {
    name: string,
    amount: number
}

interface Recipe {
    name: string,
    ingredients: Array<Ingredient>,
    servings: number
}

const basicBread: Recipe = {
    name: "Basic bread",
    ingredients: [
        {name: "Wheat flour", amount: 500},
        {name: "Tap water", amount: 300},
        {name: "Olive oil", amount: 80},
        {name: "Dry yeast", amount: 11},
        {name: "Salt", amount: 10},
        {name: "Sugar", amount: 25}
    ],
    servings: 10
}

const str = `${basicBread.name}, servings: ${basicBread.servings}`
console.log(str)
