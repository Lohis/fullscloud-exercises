class Bank {
    name: string
    accounts: Array<Account>

    nextAccountNumber: number

    constructor(name: string) {
        this.name = name
        this.accounts = []

        this.nextAccountNumber = 1
    }

    openAccount(owner: string): string {
        const newAccountId = `account${this.nextAccountNumber}`
        const newAccount = new Account(owner, newAccountId)
        this.accounts.push(newAccount)
        this.nextAccountNumber++
        return newAccountId
    }

    closeAccount(id: string) {
        const accountToBeClosed = this.accounts.find((account) => account.id === id)
        if (!accountToBeClosed) return null
        
        this.accounts = this.accounts.filter((account) => {
            if (account.id !== id) return account
        })
        
        return accountToBeClosed
    }

    deposit(accountId: string, amount: number) {
        const targetAccount = this.getAccount(accountId)
        if (!targetAccount) return null
        
        const isSuccess = targetAccount.deposit(amount)
        if (isSuccess) return amount
        else return null
    }

    withdraw(accountId: string, amount: number) {
        const targetAccount = this.getAccount(accountId)
        if (!targetAccount) return null

        const isSuccess = targetAccount.withdraw(amount)
        if (isSuccess) return amount
        else return null
    }

    checkBalance(accountId: string) {
        const targetAccount = this.getAccount(accountId)
        if (!targetAccount) return null

        return targetAccount.balance
    }

    transfer(fromAccountId: string, toAccountId: string, amount: number) {
        const fromAccount = this.getAccount(fromAccountId)
        const toAccount = this.getAccount(toAccountId)
        if (!(fromAccount && toAccount)) return null

        const isFromSuccess = fromAccount.withdraw(amount)
        if (isFromSuccess) {
            const isToSuccess = toAccount.deposit(amount)
            if (isToSuccess) return true
        }
        return false
    }

    customers() {
        const customers: string[] = []
        this.accounts.forEach((account) => {
            if (!customers.includes(account.owner)) customers.push(account.owner)
        })
        return customers
    }

    totalValue() {
        return this.accounts.reduce((acc, cur) => {return acc += cur.balance}, 0)
    }

    getAccount(accountId: string): Account|undefined {
        return this.accounts.find((account) => account.id === accountId)
    }

    toString() {
        let str = `----------\nBANK\n${this.name}\n`
        str += `Total value: ${this.totalValue()}\n`
        str += this.accounts.reduce((acc, cur) => {
            return acc += cur.toString() + "\n"
        }, "ACCOUNTS\n")
        return str
    }
}

class Account {
    owner: string
    id: string
    balance: number

    constructor(owner: string, id: string) {
        this.owner = owner
        this.id = id
        this.balance = 0
    }

    deposit(amount: number): boolean {
        if (amount < 0) return false
        this.balance += amount
        return true
    }

    withdraw(amount: number): boolean {
        if (amount < 0) return false
        if (this.balance < amount) return false
        this.balance -= amount
        return true
    }

    toString() {
        return `Account id: ${this.id}, owner: ${this.owner}, balance: ${this.balance}` 
    }
}

const bank1 = new Bank("Kroisos Pennosen kultapankki")
const acc1id = bank1.openAccount("Aku Ankka")
const acc2id = bank1.openAccount("Touho Ankka")
const acc3id = bank1.openAccount("Tupu Ankka")
const acc4id = bank1.openAccount("Tupu Ankka")
console.log(bank1.toString())
bank1.deposit(acc1id, 500)
bank1.deposit(acc2id, 800)
bank1.deposit(acc4id, -100)
console.log(bank1.toString())
bank1.withdraw(acc1id, 50)
bank1.withdraw(acc2id, 99999999)
console.log(bank1.toString())
const closedAcc = bank1.closeAccount(acc1id)
console.log(bank1.toString())
console.log(`Closed account ${closedAcc}`)
console.log(`Balance check on Touho's account: ${bank1.checkBalance(acc2id)}`)
bank1.transfer(acc2id, acc3id, 99999999)
console.log(bank1.toString())
bank1.transfer(acc2id, acc3id, 300)
console.log(bank1.toString())
console.log(bank1.customers())
