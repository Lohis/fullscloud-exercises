const generateRandomInt = (min: number, max: number) => {
    const num = Math.floor(Math.random() * (max - min)) + min
    return Math.round(num)
}

const generateRandomLetter = () => {
    let randomLetter = String.fromCharCode(generateRandomInt(65, 91))
    if (generateRandomInt(0, 2)) randomLetter = randomLetter.toLowerCase()
    return randomLetter
}

const generateRandomSpecialCharacter = () => {
    return String.fromCharCode(generateRandomInt(33, 48))
}


const generateUsername = (firstname: string, lastname: string) => {
    const date = new Date()
    return `B${date.getFullYear().toString().slice(2)}${lastname.toLowerCase().slice(0, 2)}${firstname.toLowerCase().slice(0, 2)}`
}

const generatePassword = (firstname: string, lastname: string) => {
    const letter = generateRandomLetter()
    const specialChar = generateRandomSpecialCharacter()
    const date = new Date()
    return `${letter}${firstname.charAt(0).toLowerCase()}${lastname.charAt(lastname.length-1).toUpperCase()}${specialChar}${date.getFullYear().toString().slice(2)}`
}

const generateCredentials = (firstname: string, lastname: string): string[] => {
    const credentials: string[] = []
    credentials.push(generateUsername(firstname, lastname))
    credentials.push(generatePassword(firstname, lastname))
    return credentials
}

console.log(generateCredentials("John", "Doe"))
