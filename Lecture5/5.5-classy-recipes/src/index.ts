class Ingredient {
    name: string
    amount: number

    constructor(name: string, amount: number) {
        this.name = name
        this.amount = amount
    }
}

class Recipe {
    name: string
    ingredients: Array<Ingredient>
    servings: number

    constructor(name:string, servings: number, ingredients: Array<Ingredient>) {
        this.name = name
        this.servings = servings
        this.ingredients = ingredients
    }

    setServings(servings: number) {
        const ingredientMultiplier = servings / this.servings
        this.servings = servings
        this.ingredients.forEach((ingredient: Ingredient) => ingredient.amount *= ingredientMultiplier)
    }

    toString(): string {
        let str = `Recipe: ${this.name}, ${this.servings} servings:\n`
        this.ingredients.forEach((ingredient) => str += `${ingredient.name}, ${ingredient.amount} grams\n`)
        return str
    }
}

const basicBread = new Recipe("Basic bread", 10, [
    {name: "Wheat flour", amount: 500},
    {name: "Tap water", amount: 300},
    {name: "Olive oil", amount: 80},
    {name: "Dry yeast", amount: 11},
    {name: "Salt", amount: 10},
    {name: "Sugar", amount: 25}
])

console.log(basicBread.toString())
console.log("--------------------")
basicBread.setServings(5)
console.log(basicBread.toString())
console.log("--------------------")
basicBread.setServings(15)
console.log(basicBread.toString())
