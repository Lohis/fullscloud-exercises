import fs from "fs"

const correntAnswerData = fs.readFileSync("./data/correct_answers.txt", "utf-8")
const studentAnswerData = fs.readFileSync("./data/student_answers.txt", "utf-8")

const correctAnswerArr = correntAnswerData.split(",")
const studentAnswerArr = studentAnswerData.split("\n")

const scores: number[] = studentAnswerArr.map((answer) => {
    let correctAmount = 0
    let incorrectAmount = 0
    const answerArr = answer.split(",")
    for (let i = 0; i < answerArr.length; i++) {
        if (answerArr[i] === correctAnswerArr[i]) correctAmount++
        else if (answerArr[i] !== correctAnswerArr[i] && answerArr[i].trim().length > 0) incorrectAmount++
    }
    const score = correctAmount * 4 - incorrectAmount
    if (score > 0) return score
    else return 0
})

const writeStream = fs.createWriteStream("./data/student_scores.txt")
scores.forEach((score) => writeStream.write(score + "\n"))
