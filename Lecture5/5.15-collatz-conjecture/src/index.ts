const collatzConjecture = (n: number): number => {
    let steps = 0
    while (n !== 1 && steps < 100000000) {
        if (n % 2 === 0) {
            n = n / 2
        } else {
            n = n * 3 + 1
        }
        steps++
    }
    return steps
}

console.log(`with n = 3, n = 1 is reached in ${collatzConjecture(3)} steps`)
console.log(`with n = 13, n = 1 is reached in ${collatzConjecture(13)} steps`)
