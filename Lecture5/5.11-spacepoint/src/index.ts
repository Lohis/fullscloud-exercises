class Point {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
    }
}

class SpacePoint extends Point {
    z: number

    constructor(x: number, y: number, z: number) {
        super(x, y)
        this.z = z
    }
}

class Coordinate3d {
    point3d: SpacePoint

    constructor(x?: number, y?: number, z?: number) {
        
        //coordinate constructor parameters are optional - if not given, set them as 0
        x = x ? x : 0
        y = y ? y : 0
        z = z ? z : 0

        this.point3d = new SpacePoint(x, y, z)
    }

    moveNorth(distance: number) {
        this.point3d.y += distance
    }

    moveEast(distance: number) {
        this.point3d.x += distance
    }

    moveSouth(distance: number) {
        this.point3d.y -= distance
    }

    moveWest(distance: number) {
        this.point3d.x -= distance
    }

    moveUp(distance: number) {
        this.point3d.z += distance
    }

    moveDown(distance: number) {
        this.point3d.z -= distance
    }

    toString() {
        return `x: ${this.point3d.x}, y: ${this.point3d.y}, z: ${this.point3d.z}`
    }
}

const coord1 = new Coordinate3d()
console.log(coord1.toString())
coord1.moveNorth(30)
coord1.moveEast(30)
coord1.moveSouth(31)
coord1.moveWest(29)
coord1.moveUp(10)
coord1.moveDown(5)
console.log(coord1.toString())

const coord2 = new Coordinate3d(-1000, -999, -998)
console.log(coord2.toString())
