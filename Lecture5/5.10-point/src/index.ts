class Point {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
    }
}

class Coordinate {
    point: Point

    constructor(x?: number, y?: number) {
        
        //coordinate constructor parameters are optional - if not given, set them as 0
        x = x ? x : 0
        y = y ? y : 0

        this.point = new Point(x, y)
    }

    moveNorth(distance: number) {
        this.point.y += distance
    }

    moveEast(distance: number) {
        this.point.x += distance
    }

    moveSouth(distance: number) {
        this.point.y -= distance
    }

    moveWest(distance: number) {
        this.point.x -= distance
    }

    toString() {
        return `x: ${this.point.x}, y: ${this.point.y}`
    }
}

const coord1 = new Coordinate()
console.log(coord1.toString())
coord1.moveNorth(30)
coord1.moveEast(30)
coord1.moveSouth(31)
coord1.moveWest(29)
console.log(coord1.toString())

const coord2 = new Coordinate(-1000, -999)
console.log(coord2.toString())