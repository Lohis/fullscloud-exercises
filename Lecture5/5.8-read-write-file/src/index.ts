import fs from "fs"

const song = fs.readFileSync("./joulu-on-taas.txt", "utf-8")
const songWordByWord = song.split(/\n| /) //regex split either by space or newline

let songAltered = songWordByWord.reduce((acc, cur) => {
    cur = cur.trim()
    const hasCapitalLetter = cur.charAt(0) === cur.charAt(0).toUpperCase()
    let str = ""
    if (cur.toLowerCase() === "joulu") str = "kinkku"
    else if (cur.toLowerCase() === "lapsilla") str = "poroilla"
    else str = cur
    if (hasCapitalLetter) str = str.charAt(0).toUpperCase() + str.slice(1)
    return acc += str + " "
}, "")

songAltered = songAltered.trim()
console.log(songAltered)

fs.writeFileSync("./joulu-on-taas-altered.txt", songAltered, "utf-8")