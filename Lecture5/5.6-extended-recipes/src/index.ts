class Ingredient {
    name: string
    amount: number

    constructor(name: string, amount: number) {
        this.name = name
        this.amount = amount
    }
}

class Recipe {
    name: string
    ingredients: Array<Ingredient>
    servings: number

    constructor(name:string, servings: number, ingredients: Array<Ingredient>) {
        this.name = name
        this.servings = servings
        this.ingredients = ingredients
    }

    setServings(servings: number): void {
        const ingredientMultiplier = servings / this.servings
        this.servings = servings
        this.ingredients.forEach((ingredient: Ingredient) => ingredient.amount *= ingredientMultiplier)
    }

    toString(): string {
        let str = `Recipe: ${this.name}, ${this.servings} servings\n`
        this.ingredients.forEach((ingredient) => str += `${ingredient.name}, ${ingredient.amount} grams\n`)
        return str
    }
}

class HotRecipe extends Recipe {
    heatLevel: number

    constructor(name: string, servings: number, heatLevel: number, ingredients: Array<Ingredient>) {
        super(name, servings, ingredients)
        this.heatLevel = heatLevel
    }

    toString(): string {
        let str = ""
        if (this.heatLevel >= 5) str += `Warning! This recipe is hot! Heat: ${this.heatLevel}\n`
        str += super.toString()
        return str
    }
}

const mildBread = new HotRecipe("Mild bread", 10, 3, [
    {name: "Wheat flour", amount: 500},
    {name: "Tap water", amount: 300},
    {name: "Olive oil", amount: 80},
    {name: "Dry yeast", amount: 11},
    {name: "Salt", amount: 10},
    {name: "Sugar", amount: 25}
])

console.log(mildBread.toString())
console.log("--------------------")
console.log(mildBread.setServings(100))
console.log(mildBread.toString())
console.log("--------------------")

const chiliBread = new HotRecipe("Chili bread", 10, 8, [
    {name: "Wheat flour", amount: 500},
    {name: "Tap water", amount: 300},
    {name: "Olive oil", amount: 80},
    {name: "Dry yeast", amount: 11},
    {name: "Salt", amount: 10},
    {name: "Sugar", amount: 25},
    {name: "Chili powder", amount: 10}
])

console.log(chiliBread.toString())
console.log("--------------------")
console.log(chiliBread.setServings(100))
console.log(chiliBread.toString())
console.log("--------------------")