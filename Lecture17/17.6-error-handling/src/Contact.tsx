import { useLoaderData } from "react-router-dom"
import { contacts } from "./contactProvider"

// eslint-disable-next-line @typescript-eslint/no-explicit-any, react-refresh/only-export-components
export const loader = ({params}: any) => {
    const id = Number(params.id)
    return id
} 

const Contacts = () => {
    const id = useLoaderData()
    const contact = contacts.find(contact => contact.id === id)
    if (typeof contact === "undefined") {
        throw new Error("Contact not found")
    }

    return (
        <>
            <h1>Contact</h1>
            <div><i>Name:</i> {contact.name}</div>
            <div><i>Phone:</i> {contact.phone}</div>
            <div><i>Email:</i> {contact.email}</div>
        </>
    )
}

export default Contacts