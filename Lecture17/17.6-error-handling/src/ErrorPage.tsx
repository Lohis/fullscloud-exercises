import { useRouteError, Link, isRouteErrorResponse } from 'react-router-dom'

export default function ErrorPage() {
    const error = useRouteError() as Error

    if (!isRouteErrorResponse(error)) {

        if (error.message === "Contact not found") {
            return (
                <h1>No contact found</h1>
            )
        }

        return (
            <div className='ErrorPage'>
                <h1>Unknown error</h1>
                <p>No further info available</p>
            </div>
        )
    }

    if (error.status === 404) return (
        <div className='ErrorPage'>
            <h1>404 - Not Found</h1>
            <p>The requested item is not here</p>
            <Link to={'/'}>Main Page</Link>
        </div>
    )
    console.error(error)
    return (
        <div className='ErrorPage'>
            <h1>Unexpected error</h1>
            <p>({error.status}) {error.statusText}</p>
            {error.data?.message && <p>{error.data.message}</p>}
        </div>
    )
}