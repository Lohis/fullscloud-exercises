import express, { Request, Response } from "express"
import { songList } from "./songProvider"

const server = express()
const port = 3001

server.use("/", express.static("./dist/client"))

server.get("/songs", (req: Request, res: Response) => {
    const songs = songList.map(song => {
        return { id: song.id, title: song.title }
    })
    res.send(songs)
})

server.get("/songs/:id", (req: Request, res: Response) => {
    const song = songList.find(song => song.id === Number(req.params.id))
    if (typeof song === "undefined") return res.sendStatus(404)
    res.send(song)
})

server.get("*", (req, res) => {
    res.sendFile("index.html", { root: "./dist/client" })
})

server.listen(port, () => console.log("Server listening to port", port))
