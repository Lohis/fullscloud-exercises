import axios from "axios"
//import songs from "./assets/songs"

const serverUrl = "http://localhost:3001"


export interface Song {
    id: number
    title: string
    lyrics: string
}

export const placeholderSong = {id: 0, title: "", lyrics: ""} as Song
//export const songList = songs as Song[]

export const getSongs = async () => {
    const songs = await axios.get(`${serverUrl}/songs`)
    return songs
}

export const getSong = async (id: number) => {
    const song = await axios.get(`${serverUrl}/songs/${id}`)
    return song
}
