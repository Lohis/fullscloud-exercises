import { useEffect, useState } from "react"
import { Link, Outlet } from "react-router-dom"
import axios from "axios"
import { placeholderSong } from "./songProvider"

/*
NOTE: the implementation seems to have a performance issue
*/

const SongBook = () => {
    const [activeSong, setActiveSong] = useState(0)
    const [songs, setSongs] = useState([placeholderSong])
    const [searchString, setSearchString] = useState("")
 
    useEffect(() => {
        axios.get("/songs")
        .then(response => setSongs(response.data))
    }, [])

    const filteredSongs = songs.filter(song => song.title.includes(searchString))

    return (
        <div className="SongBook">
            <h1>Song book</h1>
            <legend>Search</legend>
            <input value={searchString} onChange={(e) => {setSearchString(e.target.value)}}></input>
            <br />
            <br />
            <nav>
                {filteredSongs.map(song => {
                    if (song.id === activeSong) {
                        return (
                            <Link style={{backgroundColor: "yellow"}} onClick={() => { setActiveSong(song.id) }} to={`songs/${song.id}`}>{song.title} </Link>
                        )
                    } else {
                        return (
                            <Link onClick={() => { setActiveSong(song.id) }} to={`songs/${song.id}`}>{song.title} </Link>
                        )
                    }
                })}
            </nav>

            <Outlet />

        </div>
    )
}

export default SongBook
