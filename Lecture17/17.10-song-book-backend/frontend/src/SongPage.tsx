import { useLoaderData } from "react-router-dom"
import { placeholderSong } from "./songProvider"
import { useState } from "react"
import axios from "axios"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const loader = ({params}: any) => {
    const songId = Number(params.id)
    return songId
} 

const SongPage = () => {
    const [song, setSong] = useState(placeholderSong)

    const songIdToLoad = useLoaderData() as number
    axios.get(`/songs/${songIdToLoad}`)
        .then(response => setSong(response.data))

    return (
        <div className="SongPage">
            <h1>{song.title}</h1>
            <p>{song.lyrics}</p>
        </div>
    )
}

export default SongPage