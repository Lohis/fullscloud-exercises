import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import App from './App'
import Contacts, {loader as contactsLoader} from './Contacts'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
    },
    {
        path: '/contacts/:id',
        element: <Contacts />,
        loader: contactsLoader
    }
])

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)