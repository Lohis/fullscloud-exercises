import { useLoaderData } from "react-router-dom"

const testContact1 = {
    id: 1,
    name: "Leena Niemelä",
    email: "leeniem@spostitoo.sa",
    phone: "04714839577245",
}

const testContact2 = {
    id: 2,
    name: "Leo Nieminen",
    email: "leoniemine@postiloo.ta",
    phone: "05024783572835",
}

const testContact3 = {
    id: 333,
    name: "Teo Testilä",
    email: "testiteo@sahkopo.sti",
    phone: "0454363735735753",
}

const contacts = [testContact1, testContact2, testContact3]

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const loader = ({params}: any) => {
    const id = Number(params.id)
    return id
} 

const Contacts = () => {
    const id = useLoaderData()
    const contact = contacts.find(contact => contact.id === id)
    if (typeof contact === "undefined") {
        return (
            <h1>No contact found</h1>
        )
    }

    return (
        <>
            <h1>Contact</h1>
            <div><i>Name:</i> {contact?.name}</div>
            <div><i>Phone:</i> {contact?.phone}</div>
            <div><i>Email:</i> {contact?.email}</div>
        </>
    )
}

export default Contacts