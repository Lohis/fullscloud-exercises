import express, {Request, Response} from "express"

const router = express.Router()

router.get("/", (_req: Request, res: Response) => {
    res.send("version 0.0001")
})

export default router