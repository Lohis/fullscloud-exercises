import { Link, Outlet } from "react-router-dom"
import { contacts } from "./contactProvider"

const App = () => {
    return (
        <div>
            <h1>Contacts</h1>
            <nav>
                {contacts.map(contact => {
                    return (
                        <Link to={`contacts/${contact.id}`}>{contact.name} </Link>
                    )
                })}
            </nav>

            <Outlet />

        </div>
    )
}

export default App
