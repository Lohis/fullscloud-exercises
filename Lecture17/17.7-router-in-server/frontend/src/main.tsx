import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import App from './Contacts'
import Contact, {loader as contactsLoader} from './Contact'
import ErrorPage from './ErrorPage'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
        errorElement: <ErrorPage />,
        children: [
            {
                path: '/contacts/:id',
                element: <Contact />,
                loader: contactsLoader
            }
        ]
    }
])

const element = document.getElementById('root') as HTMLElement
ReactDOM.createRoot(element).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)

