import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import App from './App'
import Contacts from './Contacts'

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
    },
    {
        path: '/contacts',
        element: <Contacts />,
    }
])
ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)