import { useState } from "react"
import { Link, Outlet } from "react-router-dom"
import { songList } from "./songProvider"

const SongBook = () => {
    const [activeSong, setActiveSong] = useState(0)
    const [searchString, setSearchString] = useState("")

    const songs = songList.filter(song => song.title.toLowerCase().includes(searchString.toLowerCase()))

    return (
        <div className="SongBook">
            <h1>Song book</h1>
            <legend>Search</legend>
            <input value={searchString} onChange={(e) => {setSearchString(e.target.value)}}></input>
            <br />
            <br />
            <nav>
                {songs.map(song => {
                    if (song.id === activeSong) {
                        return (
                            <Link style={{backgroundColor: "yellow"}} onClick={() => { setActiveSong(song.id) }} to={`songs/${song.id}`}>{song.title} </Link>
                        )
                    } else {
                        return (
                            <Link onClick={() => { setActiveSong(song.id) }} to={`songs/${song.id}`}>{song.title} </Link>
                        )
                    }
                })}
            </nav>

            <Outlet />

        </div>
    )
}

export default SongBook
