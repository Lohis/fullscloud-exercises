import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import SongBook from './SongBook'
import SongPage, {loader as songLoader} from "./SongPage"
import ErrorPage from './ErrorPage'

const router = createBrowserRouter([
    {
        path: "/",
        element: <SongBook />,
        errorElement: <ErrorPage />,
        children: [
            {
                path: "/songs/:id",
                element: <SongPage />,
                loader: songLoader
            }
        ]
    },
])

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>,
)






