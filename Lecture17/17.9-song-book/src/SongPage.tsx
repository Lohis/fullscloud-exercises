import { useLoaderData } from "react-router-dom"
import { Song, songList } from "./songProvider"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const loader = ({params}: any): Song => {
    const songId = Number(params.id)
    const existingSong = songList.find(song => song.id === songId)
    if (typeof existingSong === "undefined") {
        throw Error("Song not found")
    }
    return existingSong
} 

const SongPage = () => {

    const song = useLoaderData() as Song

    return (
        <div className="SongPage">
            <h1>{song.title}</h1>
            <p>{song.lyrics}</p>
        </div>
    )
}

export default SongPage