const testContact1 = {
    id: 1,
    name: "Leena Niemelä",
    email: "leeniem@spostitoo.sa",
    phone: "04714839577245",
}

const testContact2 = {
    id: 2,
    name: "Leo Nieminen",
    email: "leoniemine@postiloo.ta",
    phone: "05024783572835",
}

const testContact3 = {
    id: 333,
    name: "Teo Testilä",
    email: "testiteo@sahkopo.sti",
    phone: "0454363735735753",
}

export const contacts = [testContact1, testContact2, testContact3]