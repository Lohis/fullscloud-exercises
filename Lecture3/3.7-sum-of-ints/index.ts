const n = 5

let sum1 = 0
for (let i = 1; i <= n; i++) {
    sum1 += i
}
console.log(sum1)

let sum2 = 0
let i = 1
while(i <= n) {
    sum2 += i
    i++
}
console.log(sum2)