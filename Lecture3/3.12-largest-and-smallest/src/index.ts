//using a custom NamedNumber type that ties the number's name and value together
type NamedNumber = {
    name: string
    value: number
}

const numArr: NamedNumber[] = []

for (let i = 1; i <= 3; i++) {
    const namedNum: NamedNumber = {
        name: `number${i}`,
        value: Number.parseFloat(process.argv[i + 1])
    }
    numArr.push(namedNum)
}

let output = ""
if (numArr[0].value === numArr[1].value && numArr[1].value === numArr[2].value) {
    output = "all numbers are equal"
} else {
    numArr.sort((a, b) => a.value - b.value) //using custom compare function
    output = `the smallest one: ${numArr[0].name} with value ${numArr[0].value}\nand the largest one: ${numArr[numArr.length-1].name} with value ${numArr[numArr.length-1].value}`
}
console.log(output)