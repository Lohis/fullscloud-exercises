const n = 17

let sum1 = 0
for (let i = 1; i <= n; i++) {
    if (i % 3 === 0 || i % 5 === 0) {
        sum1 += i
    }
}
console.log(sum1)