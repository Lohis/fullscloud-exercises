let str = process.argv[2]

const lastSpaceIndex: number = str.lastIndexOf(" ")
if (lastSpaceIndex !== -1) {
    str = str.slice(0, lastSpaceIndex)
}

console.log(str)