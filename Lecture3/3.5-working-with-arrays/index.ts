const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"]

console.log("3rd item: " + arr[2])
console.log("5th item: " + arr[4])
console.log("Array length: " + arr.length)

arr.sort()
console.log(arr)

arr.push("sipuli")
console.log(arr)

arr.shift()
console.log(arr)

arr.forEach(function (item) {
    console.log(item)
})

arr.forEach(function (item) {
    if (item.includes("r")) {
        console.log(item)
    }
})
