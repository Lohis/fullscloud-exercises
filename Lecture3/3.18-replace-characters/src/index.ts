const toBeReplaced = process.argv[2]
const replacement = process.argv[3]
let str = process.argv[4]

const regex = new RegExp(toBeReplaced, "g")
if (str) str = str.replace(regex, replacement)

console.log(str)
