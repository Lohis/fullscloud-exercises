const nameArr: string[] = process.argv.slice(2, 5)

const takeFirstLetterLowercase = (str: string) => {
    if (str && str.length > 0) {
        return str.charAt(0).toLowerCase()
    } else {
        return ""
    }
}

console.log(`${takeFirstLetterLowercase(nameArr[0])}.${takeFirstLetterLowercase(nameArr[1])}.${takeFirstLetterLowercase(nameArr[2])}`)