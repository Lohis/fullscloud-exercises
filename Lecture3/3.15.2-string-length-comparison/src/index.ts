const nameArr: string[] = process.argv.slice(2, 5)
nameArr.sort((a, b) => b.length - a.length) //custom compare function: compares string lengths, longest is placed first
console.log(nameArr)