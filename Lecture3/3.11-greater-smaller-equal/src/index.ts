/*
Notes about passing the arguments:
-negative numbers don't seem to work, I guess Node interprets them as flags due to the dash character
-the secret third string argument needs to be given in paratheses, otherwise it gets divided to separate args due to space character
*/
const a: number = Number.parseFloat(process.argv[2])
const b: number = Number.parseFloat(process.argv[3])
const c: string = process.argv[4]

//console.log(`Arguments: ${a} ${b} ${c}`)

let output: string = ""
if (a > b) {
    output = a + " is greater"
} else if (a < b) {
    output = b + " is greater"
} else if (a === b) {
    if (c === "hello world") {
        output = "yay, you guessed the password"
    } else {
        output = "they are equal"
    }
}
console.log(output)