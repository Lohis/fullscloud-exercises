const argument = process.argv[2]
const str = process.argv[3]

let output = ""

if (argument === "upper") {
    output = str.toUpperCase()
} else if (argument === "lower") {
    output = str.toLowerCase()
} else {
    output = str
}

console.log(output)