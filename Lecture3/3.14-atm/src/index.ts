const balance: number = 499
const isActive = true
const checkBalance = true

let output = ""

if (checkBalance) {
    if (isActive && balance > 0) {
        output = `balance: ${balance}`
    } else if (!isActive) {
        output = "your account is not active"
    } else if (balance === 0) {
        output = "your account is empty"
    } else {
        output = "your balance is negative"
    }
} else {
    output = "Have a nice day!"
}

console.log(output)
