const languageCode: string = process.argv[2]

const helloRecord: Record<string, string> = {
    "en": "Hello World", //An entry with "en" key is used as the default later, has to be present!
    "fi": "Hei maailma",
    "es": "Hola Mundo",
    "de": "Hallo Welt",
    "fr": "Bonjour le monde",
    "it": "Ciao mondo",
    "se": "Hej världen",
    "no": "Hallo verden"
}

const output: string = helloRecord[languageCode] ? helloRecord[languageCode] : helloRecord["en"] //use en as default language
console.log(output)
