let seq1 = "1. "
for (let i = 0; i <= 10; i++) {
    seq1 += (i * 100) + " "
}
console.log(seq1)

let seq2 = "2. "
for (let i = 0; i < 8; i++) {
    const n = Math.pow(2, i)
    seq2 += n + " "
}
console.log(seq2)

let seq3 = "3. "
for (let i = 1; i <= 5; i++) {
    seq3 += (i * 3) + " "
}
console.log(seq3)

let seq4 = "4. "
for (let i = 9; i >= 0; i--) {
    seq4 += i + " "
}
console.log(seq4)

let seq5 = "5. "
for (let i = 1; i <= 4; i++) {
    for (let j = 0; j < 3; j++) {
        seq5 += i + " "
    }
}
console.log(seq5)

let seq6 = "6. "
for (let i = 0; i < 3; i++) {
    for (let j = 0; j <= 4; j++) {
        seq6 += j + " "
    }
}
console.log(seq6)