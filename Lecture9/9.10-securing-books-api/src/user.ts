export default class User {
    username: string
    hash: string

    constructor(username: string, hash: string) {
        this.username = username
        if (hash === "") console.log("Error: hash is empty. The user will be unable to login")
        this.hash = hash
    }
}