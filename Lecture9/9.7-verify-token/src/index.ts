import "dotenv/config"
import jwt, { JwtPayload } from "jsonwebtoken"

const secret = process.env.SECRET ?? ""
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Imtpc3Nha2lzc2FraXNzYSIsImlhdCI6MTcwNTMyNDQ5NCwiZXhwIjoxNzA1MzI1Mzk0fQ.O_mG88LhMA_oXjIn0aMhIUoF1bofBrmVDjni3L-i0hs"

let verifiedToken: string | JwtPayload = ""
try {
    verifiedToken = jwt.verify(token, secret)
} catch (error) {
    console.log("JWT error", error)  
}

console.log(verifiedToken)
