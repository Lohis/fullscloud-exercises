import express, { Request, Response } from "express"
import argon2 from "argon2"
import { validateUserRegistration } from "./middleware"

const router = express.Router()

interface User {
    username: string
    hash: string
}

const users: User[] = []

router.post("/", validateUserRegistration, async (req: Request, res: Response) => {
    const { username, password } = req.body

    const hash = await argon2.hash(password)

    const newUser = new Object as User
    newUser.username = username
    newUser.hash = hash
    users.push(newUser)

    console.log(newUser)
    
    res.sendStatus(201)
})

export default router