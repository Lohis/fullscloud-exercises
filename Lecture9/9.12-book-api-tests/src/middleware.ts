import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken"

interface CustomRequest extends Request {
    user: string
}

interface AdminToken {
    username: string
    isAdmin: boolean
}

export const authenticateAdmin = (req: Request, res: Response, next: NextFunction) => {
    const customReq = req as CustomRequest
    const auth = customReq.get("Authorization")
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Cannot proceed: authentication failed")
    }
    const token = auth.substring(7)
    const secret = process.env.SECRET
    if (typeof secret !== "string" || secret === "") return res.sendStatus(500)

    try {
        const decodedToken = jwt.verify(token, secret)
        console.log("Decoded jwt token", decodedToken)
        
        let isAdminVerificationOk = true
        if (typeof decodedToken === "string") isAdminVerificationOk = false
        const { username, isAdmin } = decodedToken as AdminToken
        if (typeof username !== "string" && username !== "admin") isAdminVerificationOk = false
        if (typeof isAdmin !== "boolean" && !isAdmin) isAdminVerificationOk = false
        if (!isAdminVerificationOk) return res.status(401).send("Cannot proceed: authentication failed")
        
        next()
    } catch (error) {
        return res.status(401).send("Cannot proceed: authentication failed")
    }
}

export const authenticateUser = (req: Request, res: Response, next: NextFunction) => {
    const customReq = req as CustomRequest
    const auth = customReq.get("Authorization")
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Cannot proceed: authentication failed")
    }
    const token = auth.substring(7)
    const secret = process.env.SECRET
    if (typeof secret !== "string" || secret === "") return res.sendStatus(500)

    try {
        const decodedToken = jwt.verify(token, secret)
        console.log("Decoded jwt token", decodedToken)
        next()
    } catch (error) {
        return res.status(401).send("Cannot proceed: authentication failed")
    }
}

export const handleUnknownEndpoint = (req: Request, res: Response) => {
    res.sendStatus(404)
}

export const validateUsersPostReq = (req: Request, res: Response, next: NextFunction) => {  
    const { username, password } = req.body

    let isRequestOk = true
    if (!username || !password) isRequestOk = false
    if (typeof username !== "string" || typeof password !== "string") isRequestOk = false
    if (username === "" || password === "") isRequestOk = false
    if (!isRequestOk) return res.status(400).send("Cannot proceed: invalid request")

    next()
}

export const validateBooksPostReq = (req: Request, res: Response, next: NextFunction) => {
    const { name, author } = req.body

    let isRequestOk = true
    if (!name || !author) isRequestOk = false
    if (typeof name !== "string" || typeof author !== "string") isRequestOk = false
    if (name === "" || author ==="") isRequestOk = false
    if (!isRequestOk) return res.status(400).send("Cannot add: invalid request")

    next()
}

export const validateBooksPutReq = (req: Request, res: Response, next: NextFunction) => {
    const { name, author, read } = req.body
    
    let isRequestOk = true
    if (!name && !author && !read) isRequestOk = false
    if (typeof name !== "string" && typeof author !== "string" && typeof read !== "string") isRequestOk = false
    if (name === "" && author === "" && read === "") isRequestOk = false
    if (!isRequestOk) return res.status(400).send("Cannot update: invalid request")

    next()
}