import server from "../src/bookregistry"
import request from "supertest"
import { apiPath, version, usersEndpoint } from "../src/bookregistry-config"
import "dotenv/config"

//Register an user in server
beforeAll(async () => {
    await request(server).post(`/${apiPath}/${version}/${usersEndpoint}/register`).send({username: "Touho", password: "kissa13"})
})

describe("Test generic responses", () => {
    it("Returns 404 on invalid address", async () => {
        const response = await request(server)
            .get("/invalidaddress")
        expect(response.statusCode).toBe(404)
    })
})

describe("Test users route - user registration", () => {
    it("Returns 400 when trying to register an existing username", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/register`).send({username: "Touho", password: "kissa1337"})
        expect(response.statusCode).toBe(400)
        expect(response.text).toEqual("Cannot register: user already exists")
    })
    it("Returns 400 when trying to register with an username reserved for admin account", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/register`).send({username: process.env.BOOKREGISTRY_ADMIN_USERNAME, password: "kissa1337"})
        expect(response.statusCode).toBe(400)
        expect(response.text).toEqual("Cannot register: user already exists")
    })
    it("Returns 201 when registering a new user successfully", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/register`).send({username: "AnotherUser", password: "kissa1337"})
        expect(response.statusCode).toBe(201)
    })
    it("Returns 400 when user reqistration request has malformed body", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/register`).send({whatisthis: "iDunno"})
        expect(response.statusCode).toBe(400)
        expect(response.text).toEqual("Cannot proceed: invalid request")
    })
})

describe("Test users route - user login", () => {
    it("Returns 400 when trying to login with a nonexisting username", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/login`).send({username: "NonExistingUser", password: "iDontHaveAPassword"})
        expect(response.statusCode).toBe(400)
        expect(response.text).toEqual("Invalid login properties")
    })
    it("Returns 400 when an existing user tries to login with a wrong password", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/login`).send({username: "Touho", password: "ThisIsDefinitelyAWrongPassword"})
        expect(response.statusCode).toBe(400)
        expect(response.text).toEqual("Invalid login properties")
    })
    it("Returns 400 when user login request has malformed body", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/register`).send({whatisthis: "iDunnoLol"})
        expect(response.statusCode).toBe(400)
        expect(response.text).toEqual("Cannot proceed: invalid request")
    })
    it("Returns 204 when user logins successfully", async () => {
        const response = await request(server)
            .post(`/${apiPath}/${version}/${usersEndpoint}/login`).send({username: "Touho", password: "kissa13"})
        expect(response.statusCode).toBe(200)
    })
})
