import server from "./bookregistry"
import { port } from "./bookregistry-config"

server.listen(port, () => console.log("Listening to port", port))
