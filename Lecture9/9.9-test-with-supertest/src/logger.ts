import { Request, Response, NextFunction } from "express"
import { LogEntry } from "./logentry"
import fs from "fs"


const logEntries: LogEntry[] = []
const writeStream = fs.createWriteStream("./logs/bookregistry.log")

export const logRequest = (req: Request, res: Response, next: NextFunction) => {
    const eventType = "Request"
    const url = req.url
    const method = req.method
    const reqBody = req.body
    const event = `${method} ${url} ${JSON.stringify(reqBody)}`

    const newLogEntry = new LogEntry(eventType, event)
    logEntries.push(newLogEntry)

    writeStream.write(newLogEntry.toString() + "\n")
    
    next()
}

export const logInternalMsg = (msg: string) => {
    const eventType = "Internal"
    const newLogEntry = new LogEntry(eventType, msg)
    logEntries.push(newLogEntry)
    writeStream.write(newLogEntry.toString() + "\n")
}
