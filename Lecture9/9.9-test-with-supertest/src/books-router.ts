import express, {Request, Response} from "express"
import { authenticateAdmin, authenticateUser, validateBooksPostReq, validateBooksPutReq } from "./middleware"
import Book from "./book"

const router = express.Router()

let books: Book[] = []

router.get("/", authenticateUser, (req: Request, res: Response) => {
    res.status(200).send(books)
})

router.get("/:id", authenticateUser, (req: Request, res: Response) => {
    const id = req.params.id
    const book = books.find(book => book.id === Number(id))
    if (book === undefined) {
        const status = 400
        const msg = "Book with specified identifier not found"
        return res.status(status).send(msg)
    }
    res.status(200).send(book)
})

router.post("/", validateBooksPostReq, authenticateAdmin, (req: Request, res: Response) => {
    const { name, author } = req.body
    const newBook = new Book(name, author)
    books.push(newBook)
    res.sendStatus(201)
})

router.delete("/:id", authenticateAdmin, (req: Request, res: Response) => {
    const id = Number(req.params.id)

    const bookToDelete = books.find(book => book.id === id)
    if (bookToDelete === undefined) {
        const status = 400
        const msg = "Cannot delete: book with specified identifier not found"
        return res.status(status).send(msg)
    }

    books = books.filter(book => book.id !== id)
    res.sendStatus(200)
})

router.put("/:id", validateBooksPutReq, authenticateAdmin, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const { name, author, read } = req.body
    let isBodyParamsOk = true
    if (typeof name !== "string" && typeof author !== "string" && typeof read !== "string") {
        isBodyParamsOk = false
    }
    if (name === "" && author === "" && read === "") isBodyParamsOk = false
    if (!isBodyParamsOk) {
        const status = 400
        const msg = "Cannot update: not any known properties provided"
        return res.status(status).send(msg) 
    }

    const bookToUpdate = books.find(book => book.id === id)
    if (bookToUpdate === undefined) {
        const status = 400
        const msg = "Cannot update: book with specified identifier not found"
        return res.status(status).send(msg)
    }

    if (read) {
        if (read === "true") bookToUpdate.read = true
        else if (read === "false") bookToUpdate.read = false
        else {
            const status = 400
            const msg = "Cannot update: invalid value provided for read property"
            return res.status(status).send(msg)
        }
    }
    if (name) bookToUpdate.name = name
    if (author) bookToUpdate.author = author

    res.sendStatus(200)
})

export default router
