
export class LogEntry {
    time: string
    eventType: string
    entry: string

    constructor(eventType: string, entry: string) {
        this.eventType = eventType
        this.entry = entry
        this.time = new Date().toISOString()
    }

    toString() {
        return `${this.eventType} ${this.time} ${this.entry}`
    }
}