import express from "express"
import { logRequest, handleUnknownEndpoint } from "./middleware"
import studentRouter from "./studentrouter"

const port = 3000
const server = express()

server.use(express.json())
server.use(logRequest)
server.use(express.static("public"))

server.use("/student", studentRouter)
server.use(handleUnknownEndpoint)

server.listen(port, () => {
    console.log("Server listening port", port)
})
