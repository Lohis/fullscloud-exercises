import express, { Request, Response } from "express"
import { validateStudentDeleteReq, validateStudentPostReq, validateStudentPutReq } from "./middleware"

const router = express.Router()

interface Student {
    id: number
    name: string
    email: string
}

let students: Student[] = []

router.post("/", validateStudentPostReq, (req: Request, res: Response) => {
    const newStudent = new Object as Student
    newStudent.id = Number(req.body.id)
    newStudent.name = req.body.name
    newStudent.email = req.body.email
    students.push(newStudent)

    res.sendStatus(201)
})

router.put("/:id", validateStudentPutReq, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find(student => student.id === id)
    if (student) {
        if (req.body["name"]) student.name = req.body["name"]
        if (req.body["email"]) student.email = req.body["email"]
        res.sendStatus(204)
    } else res.status(404).send(`Student with id ${id} not found`)
})

router.delete("/:id", validateStudentDeleteReq, (req: Request, res: Response) => {
    const idToDelete = Number(req.params.id)
    const studentToDelete = students.find(student => student.id === idToDelete)

    if (!studentToDelete) res.status(404).send(`Cannot delete: student with id ${idToDelete} not found`)

    //It would be wiser to retain the student item for now, and just set its status as inactive
    students = students.filter((student => student.id !== idToDelete))

    res.sendStatus(200)
})

router.get("/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find(student => student.id === id)

    if (!student) res.status(404).send(`Student with id ${id} not found`)
    res.send(student)
})

router.get("/", (req: Request, res: Response) => {
    const studentIds = students.map(student => student.id)
    res.send(studentIds)
})

export default router
