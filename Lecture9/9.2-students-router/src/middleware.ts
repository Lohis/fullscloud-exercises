import { Request, Response, NextFunction } from "express"

interface LogEntry {
    time: number
    method: string
    url: string
    requestbody: string
}

const logEntries: LogEntry[] = []

export const validateStudentPostReq = (req: Request, res: Response, next: NextFunction) => {
    const { id, name, email } = req.body

    if (typeof (Number(id)) !== "number" || isNaN(Number(id)) || typeof (name) !== "string" || typeof (email) !== "string") {
        return res.status(400).send("Missing or invalid parameters")
    }

    next()
}

export const validateStudentPutReq = (req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id)
    if (id && !isNaN(id)) {
        const { name, email } = req.body
        if (!(name || email)) return res.status(400).send("No update information provided")
        if (name && typeof (name) !== "string") {
            return res.status(400).send("Provided student name information is invalid")
        }
        if (email && typeof (email) !== "string") {
            return res.status(400).send("Provided student name information is invalid")
        }

        next()

    } else res.status(404).send("Not a valid identifier")
}

export const validateStudentDeleteReq = ((req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id)
    if (!id || isNaN(id)) return res.status(400).send("Missing or invalid parameters")
    next()
})

export const logRequest = (req: Request, res: Response, next: NextFunction) => {
    const logEntry = new Object as LogEntry
    logEntry.time = Date.now()
    logEntry.method = req.method
    logEntry.url = req.url
    logEntry.requestbody = req.body
    logEntries.push(logEntry)

    console.log(logEntry)

    next()
}

export const handleUnknownEndpoint = (req: Request, res: Response) => {
    res.status(404).send({ error: `unknown endpoint ${req.url}` })
}
