import { Request, Response, NextFunction } from "express"
import "dotenv/config"
import jwt from "jsonwebtoken"

interface LogEntry {
    time: number
    method: string
    url: string
    requestbody: string
}

interface CustomRequest extends Request {
    user: string
}

const logEntries: LogEntry[] = []

export const authenticate = (req: Request, res: Response, next: NextFunction) => {
    const customReq = req as CustomRequest //doing the casting explicitly here
    const auth = customReq.get("Authorization")
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Invalid token")
    }
    const token = auth.substring(7)
    const secret = process.env.SECRET
    if (!secret) return res.sendStatus(500)

    try {
        const decodedToken = jwt.verify(token, secret)
        customReq.user = decodedToken.toString()
        next()
    } catch (error) {
        return res.status(401).send("Invalid token")
    }
}
  
export const validateUserPost = (req: Request, res: Response, next: NextFunction) => {
    const { username, password } = req.body

    if (typeof username !== "string" || typeof password !== "string" || username === "" || password === "") {
        return res.status(400).send("Missing username and/or password")
    }

    next()
}

export const validateStudentPostReq = (req: Request, res: Response, next: NextFunction) => {
    const { id, name, email } = req.body

    if (typeof (Number(id)) !== "number" || isNaN(Number(id)) || typeof (name) !== "string" || typeof (email) !== "string") {
        return res.status(400).send("Missing or invalid parameters")
    }

    next()
}

export const validateStudentPutReq = (req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id)
    if (id && !isNaN(id)) {
        const { name, email } = req.body
        if (!(name || email)) return res.status(400).send("No update information provided")
        if (name && typeof (name) !== "string") {
            return res.status(400).send("Provided student name information is invalid")
        }
        if (email && typeof (email) !== "string") {
            return res.status(400).send("Provided student name information is invalid")
        }

        next()

    } else res.status(404).send("Not a valid identifier")
}

export const validateStudentDeleteReq = ((req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id)
    if (!id || isNaN(id)) return res.status(400).send("Missing or invalid parameters")
    next()
})

export const logRequest = (req: Request, res: Response, next: NextFunction) => {
    const logEntry = new Object as LogEntry
    logEntry.time = Date.now()
    logEntry.method = req.method
    logEntry.url = req.url
    logEntry.requestbody = req.body
    logEntries.push(logEntry)

    console.log(logEntry)

    next()
}

export const handleUnknownEndpoint = (req: Request, res: Response) => {
    res.status(404).send({ error: `unknown endpoint ${req.url}` })
}
