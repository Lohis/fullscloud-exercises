import express, { Request, Response } from "express"
import argon2 from "argon2"
import { validateUserPost } from "./middleware"
import "dotenv/config"
import jwt from "jsonwebtoken"

const router = express.Router()

interface User {
    username: string
    hash: string
}

const users: User[] = []

router.post("/", validateUserPost, async (req: Request, res: Response) => {
    const { username, password } = req.body

    const hash = await argon2.hash(password)

    const newUser = new Object as User
    newUser.username = username
    newUser.hash = hash
    users.push(newUser)

    console.log(`New user created: ${newUser}`)
    
    res.sendStatus(201)
})

router.post("/login", validateUserPost, async (req: Request, res: Response) => {
    const { username, password } = req.body

    const existingUser = users.find(user => user.username === username)
    if (!existingUser) return res.status(404).send("Invalid username and/or password")
    
    const isCorrectPassword = await argon2.verify(existingUser.hash, password)
    if (!isCorrectPassword) return res.sendStatus(401)

    const payload = { username: username }
    const secret = process.env.SECRET
    if (!secret) return res.sendStatus(500)
    const options = { expiresIn: "3h"}
    const token = jwt.sign(payload, secret, options)
    console.log(`Session bearer token (expires in 3h): ${token}`)

    res.status(204).send(token)
})

router.post("/admin", validateUserPost, async (req: Request, res: Response) => {
    const { username, password } = req.body
    const { STUDENTREGISTRY_ADMIN_LOGIN, STUDENTREGISTRY_ADMIN_HASH } = process.env

    if (!(STUDENTREGISTRY_ADMIN_LOGIN && STUDENTREGISTRY_ADMIN_HASH)) {
        console.log("Missing admin credentials")
        return res.sendStatus(500)
    }

    const isCorrectPassword = await argon2.verify(STUDENTREGISTRY_ADMIN_HASH, password)
    if (username !== STUDENTREGISTRY_ADMIN_LOGIN || !isCorrectPassword) return res.status(401).send("Invalid username and/or password")

    res.sendStatus(204)
})

export default router