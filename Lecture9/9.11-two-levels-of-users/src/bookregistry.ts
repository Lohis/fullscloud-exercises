import express from "express"
import helmet from "helmet"
import { port, apiPath, version, bookRegistryEndpoint, usersEndpoint } from "./bookregistry-config"
import { handleUnknownEndpoint } from "./middleware"
import { logRequest, logInternalMsg } from "./logger" 
import booksRouter from "./books-router"
import usersRouter from "./users-router"

const server = express()
server.use(express.json())
server.use(helmet())
server.use(logRequest)

server.use(`/${apiPath}/${version}/${usersEndpoint}`, usersRouter)
server.use(`/${apiPath}/${version}/${bookRegistryEndpoint}`, booksRouter)

server.use(handleUnknownEndpoint)

server.listen(port)
console.log("Listening to port", port)
logInternalMsg("Server started")
