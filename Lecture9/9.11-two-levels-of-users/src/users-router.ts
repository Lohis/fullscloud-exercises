import express, {Request, Response} from "express"
import { validateUsersPostReq } from "./middleware"
import jwt from "jsonwebtoken"
import argon2 from "argon2"
import "dotenv/config"
import User from "./user"

const router = express.Router()

const users: User[] = []

users.push(new User(process.env.BOOKREGISTRY_ADMIN_USERNAME as string, process.env.BOOKREGISTRY_ADMIN_HASH as string))

router.post("/register", validateUsersPostReq, async (req: Request, res: Response) => {
    const { username, password } = req.body

    const existingUser = users.find(user => user.username === username)
    if (existingUser) return res.status(400).send("Cannot register: user already exists")

    const hash = await argon2.hash(password)
    if (typeof hash !== "string" || hash === "") return res.sendStatus(500)

    const newUser = new User(username, hash)
    users.push(newUser)

    console.log(`New user created: ${newUser.username} ${newUser.hash}`)

    res.sendStatus(201)
})

router.post("/login", validateUsersPostReq, async (req: Request, res: Response) => {
    const { username, password } = req.body

    const existingUser = users.find(user => user.username === username)
    if (!existingUser) return res.status(400).send("Invalid login properties")

    const isLoginOk = await argon2.verify(existingUser.hash, password)
    if (!isLoginOk) return res.status(400).send("Invalid login properties")

    let payload: object = {}
    if (username === "admin") {
        payload = { username: "admin", isAdmin: true }
    } else {
        payload = { username: username }
    }
 
    const secret = process.env.SECRET
    if (typeof secret !== "string" || secret === "") return res.sendStatus(500)

    const options  = { expiresIn: "3h"}
        
    const token = jwt.sign(payload, secret, options)
    console.log(`Session bearer token (expires in 3h): ${token}`)

    res.status(204).send(token)
})

export default router
