import express, { Request, Response } from "express"
import argon2 from "argon2"
import { validateUserPost } from "./middleware"

const router = express.Router()

interface User {
    username: string
    hash: string
}

const users: User[] = []

router.post("/", validateUserPost, async (req: Request, res: Response) => {
    const { username, password } = req.body

    console.log(`Registration password: ${password}`)
    const hash = await argon2.hash(password)

    const newUser = new Object as User
    newUser.username = username
    newUser.hash = hash
    users.push(newUser)

    console.log(newUser)
    
    res.sendStatus(201)
})

router.post("/login", validateUserPost, async (req: Request, res: Response) => {
    const { username, password } = req.body

    const existingUser = users.find(user => user.username === username)
    if (!existingUser) return res.status(404).send("Invalid username and/or password")
    
    const isCorrectPassword = await argon2.verify(existingUser.hash, password)
    if (!isCorrectPassword) return res.sendStatus(401)

    res.sendStatus(204)
})



export default router