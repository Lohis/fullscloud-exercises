import "dotenv/config"
import jwt from "jsonwebtoken"
import "dotenv/config"

const payload = { username: "kissakissakissa" }
const secret = process.env.SECRET ?? ""
const options = { expiresIn: "15m" }

const token = jwt.sign(payload, secret, options)
console.log(token)