import express, {Request, Response} from "express"

const server = express()

server.get("/hello", (_req: Request, res: Response) => {
    res.send("Hello World")
})

server.use((_req: Request, res: Response) => res.sendStatus(404))

const port = 3000
server.listen(port)
console.log("Listening port", port)