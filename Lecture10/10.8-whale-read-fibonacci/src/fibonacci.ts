const timeout = 1000
let n = 0

const fibonacci = (n: number): number => {
    if (n <= 0) return 0
    if (n == 1) return 1
    
    return fibonacci(n - 2) + fibonacci(n - 1)
}

const printFibonacciContinuously = () => {
    if (n > 1000000000000000) n = 0
    console.log(fibonacci(n++))
    setTimeout(printFibonacciContinuously, timeout)
}

printFibonacciContinuously()
