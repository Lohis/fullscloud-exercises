import express, {Request, Response} from "express"
import argon2 from "argon2"
import User from "./user"
import jwt from "jsonwebtoken"
import { validateUserPostReq } from "./middleware"
import "dotenv/config"

const router = express.Router()

const users: User[] = []

router.post("/register", validateUserPostReq, async (req: Request, res: Response) => {
    const {username, password} = req.body

    const existingUser = users.find(user => user.username === username)
    if (existingUser !== undefined) return res.status(400).send("Cannot create: user already exists")

    const passwordMinLen = process.env.MIN_PASSWORD_LEN
    if (typeof passwordMinLen !== "string" || passwordMinLen === "") return res.sendStatus(500)
    if (password.length < Number(passwordMinLen)) return res.status(400).send(`Password has to be at least ${passwordMinLen} characers long`)

    const hash = await argon2.hash(password)
    const newUser = new User(username, hash)
    users.push(newUser)

    res.sendStatus(201)
})

router.post("/login", validateUserPostReq, async (req: Request, res: Response) => {
    const {username, password} = req.body

    const existingUser = users.find(user => user.username === username)
    if (existingUser === undefined) return res.status(400).send("Invalid login")

    const isLoginOk = await argon2.verify(existingUser.hash, password)
    if (!isLoginOk) return res.status(400).send("Invalid login")

    const payload = { username: username }
    
    const secret = process.env.SECRET
    if (typeof secret !== "string" || secret === "") return res.sendStatus(500)
    
    const expiration = process.env.TOKEN_EXPIRATION
    if (typeof expiration !== "string" || expiration === "") return res.sendStatus(500)
    
    const options = {expiresIn: expiration}

    const token = jwt.sign(payload, secret, options)
    console.log(`Bearer token (exp in ${expiration}): ${token}`)

    res.status(204).send(token)
})

export default router