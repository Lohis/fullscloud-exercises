import express, {Request, Response} from "express"
import Notice from "./notice"
import { validateBoardPostReq } from "./middleware"

const router = express.Router()

let notices: Notice[] = []

//endpoint for reading all notices
router.get("/", (_req: Request, res: Response) => {
    res.status(200).send(notices)
})

//endpoint for creating a notice
router.post("/", validateBoardPostReq, (req: Request, res: Response) => {
    const {message} = req.body
    const newNotice = new Notice(message, false)
    notices.push(newNotice)
    res.sendStatus(201)
})

//endpoint for deleting a notice
router.delete("/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const existingNotice = notices.find(notice => notice.id === id)
    if (existingNotice === undefined) return res.status(404).send("Notice not found for deletion")
    notices = notices.filter(notice => notice.id !== id)
    res.sendStatus(200)
})

export default router