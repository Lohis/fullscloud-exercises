import {Request, Response, NextFunction} from "express"
import jwt from "jsonwebtoken"
import "dotenv/config"

export const authenticate = (req: Request, res: Response, next: NextFunction) => {
    const auth = req.get("Authorization")
    if (!auth?.startsWith("Bearer ")) {
        return res.status(401).send("Authentication failed")
    }
    const token = auth.substring(7)
    const secret = process.env.SECRET
    if (typeof secret !== "string" || secret === "") return res.sendStatus(500)

    try {
        jwt.verify(token, secret)
        next()
    } catch (error) {
        return res.status(401).send("Authentication failed")
    }
}

export const handleUnknownEndpoint = (_req: Request, res: Response) => res.sendStatus(404)

export const validateBoardPostReq = (req: Request, res: Response, next: NextFunction) => {
    const {message} = req.body
    if (typeof message !== "string" || message === "") return res.sendStatus(400)
    next()
}

export const validateUserPostReq = (req: Request, res: Response, next: NextFunction) => {
    const {username, password} = req.body
    if (typeof username !== "string" || username === "") return res.status(400).send("Invalid username")
    if (typeof password !== "string" || password === "") return res.status(400).send("Invalid password")

    next()
}


