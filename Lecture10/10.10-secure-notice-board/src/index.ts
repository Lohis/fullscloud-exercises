import server from "./noticeboard"
import "dotenv/config"

const port = process.env.PORT
if (typeof port !== "string" || port === "") console.log("No env variable for port found")
else server.listen(port, () => console.log("Listening to port", port))