let nextNormalNoticeId = 1
let nextSecretNoticeId = 1

export default class Notice {
    id: number
    message: string
    timestamp: string

    constructor(message: string, isSecret: boolean) {
        this.message = message
        this.timestamp = new Date().toISOString()

        if (isSecret) this.id = nextSecretNoticeId++
        else this.id = nextNormalNoticeId++
    }
}