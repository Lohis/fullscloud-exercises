let nextUserId = 1

export default class User {
    id: number
    username: string
    hash: string

    constructor(username: string, hash: string) {
        this.id = nextUserId++
        this.username = username
        if (hash === "") console.log("Error: hash is empty. The user will be unable to login")
        this.hash = hash
    }
}