import express, {Request, Response} from "express"
import Notice from "./notice"
import { authenticate, validateBoardPostReq } from "./middleware"

const router = express.Router()

let secretNotices: Notice[] = []

//endpoint for reading all secret notices
router.get("/", authenticate, (_req: Request, res: Response) => {
    res.status(200).send(secretNotices)
})

//endpoint for creating a secret notice
router.post("/", authenticate, validateBoardPostReq, (req: Request, res: Response) => {
    const {message} = req.body
    const newNotice = new Notice(message, true)
    secretNotices.push(newNotice)
    res.sendStatus(201)
})

//endpoint for deleting a secret notice
router.delete("/:id", authenticate, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const existingNotice = secretNotices.find(notice => notice.id === id)
    if (existingNotice === undefined) return res.status(404).send("Notice not found for deletion")
    secretNotices = secretNotices.filter(notice => notice.id !== id)
    res.sendStatus(200)
})

export default router