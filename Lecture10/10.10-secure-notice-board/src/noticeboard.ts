import express from "express"
import { handleUnknownEndpoint } from "./middleware"
import userRouter from "./user-router"
import boardRouter from "./board-router"
import secretBoardRouter from "./secretboard-router"

const server = express()
server.use(express.json())

server.use("/user", userRouter)
server.use("/board", boardRouter)
server.use("/secretboard", secretBoardRouter)
server.use(handleUnknownEndpoint)

export default server