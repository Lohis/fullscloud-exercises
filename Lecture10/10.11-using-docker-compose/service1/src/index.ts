import express, {Request, Response} from "express"

const server = express()
const port = 3001

server.get("/", (_req: Request, res:Response) => {
    res.status(200).send("SERVICE1")
})

server.use((_req: Request, res:Response) => res.sendStatus(404))

server.listen(port, () => console.log("Listening to port", port))