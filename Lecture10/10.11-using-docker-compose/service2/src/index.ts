import express, {Request, Response} from "express"
import axios from "axios"

const server = express()

//service1 is the service name used in compose.yaml, these must match.
const service1addr = "http://service1"
const service1port = 3001
const port = 3002

server.get("/", async (_req: Request, res:Response) => {
    const service1response = await axios.get(`${service1addr}:${service1port}`)
    res.status(200).send(`${service1response.data} SERVICE2`)
})

server.use((_req: Request, res:Response) => res.sendStatus(404))

server.listen(port, () => console.log("Listening to port", port))