import express from "express"
import { handleUnknownEndpoint } from "./middleware"
import boardRouter from "./board-router"

const server = express()
server.use(express.json())

server.use("/board", boardRouter)
server.use(handleUnknownEndpoint)

export default server