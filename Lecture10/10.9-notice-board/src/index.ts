import server from "./noticeboard"

const port = 3000

server.listen(port, () => console.log("Listening to port", port))