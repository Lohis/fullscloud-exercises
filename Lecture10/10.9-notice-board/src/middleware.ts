import {Request, Response, NextFunction} from "express"

export const handleUnknownEndpoint = (_req: Request, res: Response) => res.sendStatus(404)

export const validateBoardPostReq = (req: Request, res: Response, next: NextFunction) => {
    const { message } = req.body

    let isRequestOk = true
    if (typeof message !== "string") isRequestOk = false
    if (message === "") isRequestOk = false
    if (!isRequestOk) return res.sendStatus(400)

    next()
}
