let nextNoticeId = 1

export default class Notice {
    id: number
    message: string
    timestamp: string

    constructor(message: string) {
        this.message = message
        this.id = nextNoticeId++
        this.timestamp = new Date().toISOString()
    }
}