import express, { Request, Response } from "express"
import { handleUnknownEndpoint, validateStudentArray, logRequest } from "./middleware"
import { Student, ResponseJSON } from "./interfaces"

const server = express()
const port = 3000

server.use(express.json())
server.use(logRequest) //debug printing

server.get("/checkscores", validateStudentArray, (req: Request, res: Response) => {
    const students: Student[] = req.body
    let totalScore = 0
    let highestScoringStudent = students[0]

    students.forEach((student) => {
        totalScore += student.score
        if (highestScoringStudent.score < student.score) highestScoringStudent = student
    })

    const averageScore = totalScore / students.length

    let amountOfStudentsAboveAgerage = 0
    students.forEach((student) => {
        if (student.score >= averageScore) amountOfStudentsAboveAgerage++
    })

    const responseJSON = new Object as ResponseJSON
    responseJSON.highestscoringstudent = highestScoringStudent.name
    responseJSON.averagescore = averageScore
    responseJSON.amountOfStudentAboveAverage = amountOfStudentsAboveAgerage
    res.status(200).send(JSON.stringify(responseJSON))
})

server.use(handleUnknownEndpoint)

server.listen(port, () => {
    console.log("Server listening port", port)
})
