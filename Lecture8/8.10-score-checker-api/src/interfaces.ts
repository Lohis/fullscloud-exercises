export interface Student {
    name: string
    score: number
}

export interface ResponseJSON {
    highestscoringstudent: string
    averagescore: number
    amountOfStudentAboveAverage: number
}