import { Request, Response, NextFunction } from "express"
import { Student } from "./interfaces"

export const handleUnknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: "unknown endpoint"})
}

export const logRequest = (req: Request, res: Response, next: NextFunction) => {
    console.log("Request url:", req.url)
    const reqBodyItems: object[] = req.body
    console.log("Request body:", reqBodyItems)
    next()
}

export const validateStudentArray = (req: Request, res: Response, next: NextFunction) => {
    const students: Student[] = req.body
    
    for (let i = 0; i < students.length; i++) {
        if (students[i].name === undefined || students[i].score === undefined) {
            return res.status(400).send("Missing properties on student object(s)")
        }
        if (typeof(students[i].name) !== "string" || isNaN(Number(students[i].score))) {
            return res.status(400).send("Invalid properties on student object(s)")
        }
    }
    
    console.log("Validation OK")
    next()
}
