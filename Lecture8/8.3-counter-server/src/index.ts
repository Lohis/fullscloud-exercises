import express, { Request, Response } from "express"

const server = express()
const port = 3000

let counter = 0

interface ResponseJSON {
    message: string
    count: number
}

server.get("/counter", (req: Request, res: Response) => {

    const num = Number(req.query.number)
    if (num) counter = num
    else counter++

    const responseJSON = new Object as ResponseJSON
    responseJSON.message = "Resource access count"
    responseJSON.count = counter

    res.send(responseJSON)   
})

server.listen(port)
console.log("Listening port", port)