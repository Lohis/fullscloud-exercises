import express, { Request, Response, NextFunction } from "express"

const server = express()
const port = 3000

server.use(express.json())

interface Student {
    name: string
}

interface LogEntry {
    time: number
    method: string
    url: string
    body: string
}

const logEntries: LogEntry[] = []
const students: Student[] = []

const logRequest = (req: Request, res: Response, next: NextFunction) => {
    const logEntry = new Object as LogEntry
    logEntry.time = Date.now()
    logEntry.method = req.method
    logEntry.url = req.url
    logEntry.body = req.body
    logEntries.push(logEntry)

    console.log(logEntry)
    console.log(`log entries: ${logEntries.length}`)

    next()
}

server.use(logRequest)

server.get("/students", (req: Request, res: Response) => {
    res.send(students)
})

server.listen(port, () => {
    console.log("Server listening port", port)
})
