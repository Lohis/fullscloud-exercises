import express, { Request, Response } from "express"

const server = express()
server.use(express.json())

const port = 3000
const apiPath = "api"
const version = "v1"
const bookRegistryEndpoint = "books"
let nextNewBookId = 1

class Book {
    id: number
    name: string
    author: string
    read: boolean

    constructor(name: string, author: string) {
        this.name = name
        this.author = author
        this.read = false
        this.id = nextNewBookId++
    }
}

let books: Book[] = []

server.get(`/${apiPath}/${version}/${bookRegistryEndpoint}`, (req: Request, res: Response) => {
    res.status(200).send(books)
})

server.get(`/${apiPath}/${version}/${bookRegistryEndpoint}/:id`, (req: Request, res: Response) => {
    const id = req.params.id
    const book = books.find(book => book.id === Number(id))
    if (!book) return res.status(400).send("Book with specified identifier not found")
    res.status(200).send(book)
})

server.post(`/${apiPath}/${version}/${bookRegistryEndpoint}`, (req: Request, res: Response) => {
    const { name, author } = req.body
    const newBook = new Book(name, author)
    books.push(newBook)
    res.sendStatus(201)
})

server.delete(`/${apiPath}/${version}/${bookRegistryEndpoint}/:id`, (req: Request, res: Response) => {
    const id = Number(req.params.id)

    const bookToDelete = books.find(book => book.id === id)
    if (!bookToDelete) return res.status(400).send("Cannot delete: book with specified identifier not found")

    books = books.filter(book => book.id !== id)
    res.sendStatus(200)
})

server.put(`/${apiPath}/${version}/${bookRegistryEndpoint}/:id`, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const { name, author, read } = req.body
    if (!(name || author || read)) return res.status(400).send("Cannot update: not any known properties provided") 

    const bookToUpdate = books.find(book => book.id === id)
    if (!bookToUpdate) return res.status(400).send("Cannot update: book with specified identifier not found")

    if (read) {
        if (read === "true") bookToUpdate.read = true
        else if (read === "false") bookToUpdate.read = false
        else return res.status(400).send("Cannot update: invalid value provided for read property")
    }
    if (name) bookToUpdate.name = name
    if (author) bookToUpdate.author = author

    res.sendStatus(200)
})

server.listen(port)
console.log("Listening to port", port)
