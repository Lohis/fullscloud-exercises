import { Request, Response, NextFunction } from "express"

export const handleUnknownEndpoint = (req: Request, res: Response) => {
    res.sendStatus(404)
}

export const validatePostReq = (req: Request, res: Response, next: NextFunction) => {
    const { name, author } = req.body
    if (!(name && author)) return res.status(400).send("Cannot add: missing properties")

    next()
}

export const validatePutReq = (req: Request, res: Response, next: NextFunction) => {
    const { name, author, read } = req.body
    if (!(name || author || read)) return res.status(400).send("Cannot update: not any valid properties provided")

    next()
}