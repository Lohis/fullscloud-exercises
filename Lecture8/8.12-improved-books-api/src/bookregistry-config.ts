export const port = 3000
export const apiPath = "api"
export const version = "v1"
export const bookRegistryEndpoint = "books"