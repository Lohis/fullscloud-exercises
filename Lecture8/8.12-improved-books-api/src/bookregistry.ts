import express, { Request, Response } from "express"
import helmet from "helmet"
import { Book } from "./book"
import { apiPath, version, bookRegistryEndpoint, port } from "./bookregistry-config"
import { handleUnknownEndpoint, validatePostReq, validatePutReq } from "./middleware"
import { logRequest, logInternalMsg } from "./logger" 

const server = express()
server.use(express.json())
server.use(helmet())
server.use(logRequest)

let books: Book[] = []

server.get(`/${apiPath}/${version}/${bookRegistryEndpoint}`, (req: Request, res: Response) => {
    res.status(200).send(books)
})

server.get(`/${apiPath}/${version}/${bookRegistryEndpoint}/:id`, (req: Request, res: Response) => {
    const id = req.params.id
    const book = books.find(book => book.id === Number(id))
    if (!book) {
        const status = 400
        const msg = "Book with specified identifier not found"
        return res.status(status).send(msg)
    }
    res.status(200).send(book)
})

server.post(`/${apiPath}/${version}/${bookRegistryEndpoint}`, validatePostReq, (req: Request, res: Response) => {
    const { name, author } = req.body
    const newBook = new Book(name, author)
    books.push(newBook)
    res.sendStatus(201)
})

server.delete(`/${apiPath}/${version}/${bookRegistryEndpoint}/:id`, (req: Request, res: Response) => {
    const id = Number(req.params.id)

    const bookToDelete = books.find(book => book.id === id)
    if (!bookToDelete) {
        const status = 400
        const msg = "Cannot delete: book with specified identifier not found"
        return res.status(status).send(msg)
    }

    books = books.filter(book => book.id !== id)
    res.sendStatus(200)
})

server.put(`/${apiPath}/${version}/${bookRegistryEndpoint}/:id`, validatePutReq, (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const { name, author, read } = req.body
    if (!(name || author || read)) {
        const status = 400
        const msg = "Cannot update: not any known properties provided"
        return res.status(status).send(msg) 
    }

    const bookToUpdate = books.find(book => book.id === id)
    if (!bookToUpdate) {
        const status = 400
        const msg = "Cannot update: book with specified identifier not found"
        return res.status(status).send(msg)
    }

    if (read) {
        if (read === "true") bookToUpdate.read = true
        else if (read === "false") bookToUpdate.read = false
        else {
            const status = 400
            const msg = "Cannot update: invalid value provided for read property"
            return res.status(status).send(msg)
        }
    }
    if (name) bookToUpdate.name = name
    if (author) bookToUpdate.author = author

    res.sendStatus(200)
})

server.use(handleUnknownEndpoint)

server.listen(port)
console.log("Listening to port", port)
logInternalMsg("Server started")
