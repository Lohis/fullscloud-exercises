let nextNewBookId = 1

export class Book {
    id: number
    name: string
    author: string
    read: boolean

    constructor(name: string, author: string) {
        this.name = name
        this.author = author
        this.read = false
        this.id = nextNewBookId++
    }
}