import express, { Request, Response } from "express"
import { logRequest, handleUnknownEndpoint } from "./middleware"

const server = express()
const port = 3000

server.use(express.json())

interface Student {
    name: string
}

const students: Student[] = []

server.use(logRequest)

server.get("/students", (req: Request, res: Response) => {
    res.send(students)
})

server.use(handleUnknownEndpoint)

server.listen(port, () => {
    console.log("Server listening port", port)
})
