import { Request, Response, NextFunction } from "express"

interface LogEntry {
    time: number
    method: string
    url: string
    requestbody: string
}

const logEntries: LogEntry[] = []

export const logRequest = (req: Request, res: Response, next: NextFunction) => {
    const logEntry = new Object as LogEntry
    logEntry.time = Date.now()
    logEntry.method = req.method
    logEntry.url = req.url
    logEntry.requestbody = req.body
    logEntries.push(logEntry)

    console.log(logEntry)
    console.log(`log entries: ${logEntries.length}`)
    
    next()
}

export const handleUnknownEndpoint = (_req: Request, res: Response) => {
    res.status(404).send({error: "unknown endpoint"})
}
