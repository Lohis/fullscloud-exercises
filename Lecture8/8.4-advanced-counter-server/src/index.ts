import express, { Request, Response } from "express"

const server = express()

const visitors: Record<string, number> = {}

server.get("/counter/:name", (req: Request, res: Response) => {
    const reqName = req.params.name

    if (visitors[reqName]) visitors[reqName]++
    else visitors[reqName] = 1

    res.send(`${reqName} was here ${visitors[reqName]} times`)
})

const port = 3000
server.listen(port, () => {
    console.log("Server listening port", port)
})
