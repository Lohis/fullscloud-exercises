import http, { IncomingMessage, ServerResponse } from "http"

const server = http.createServer((_req: IncomingMessage, res: ServerResponse) => {
    res.write("Hello world!")
    res.end()
})
const port = 56789
server.listen(port)
console.log("Listening port", port)