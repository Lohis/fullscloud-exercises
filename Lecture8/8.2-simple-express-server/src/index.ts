import express, { Request, Response } from "express"

const server = express()

server.get("/", (_req: Request, res: Response) => {
    res.send("Hello world!")
})

const port = 3000
server.listen(port)
console.log("Listening port", port)