import express, { Request, Response } from "express"
import { logRequest, handleUnknownEndpoint, validatePostStudent } from "./middleware"

const port = 3000
const server = express()

interface Student {
    id: number
    name: string
    email: string
}

const students: Student[] = []

server.use(express.json())
server.use(logRequest)

server.post("/student", validatePostStudent, (req: Request, res: Response) => {
    const newStudent = new Object as Student
    newStudent.id = Number(req.body.id)
    newStudent.name = req.body.name
    newStudent.email = req.body.email
    students.push(newStudent)

    res.sendStatus(201)
})

server.get("/student/:id", (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const student = students.find(student => student.id === id)

    if (!student) res.status(404).send(`Student with id ${id} not found`)
    res.send(student)
})

server.get("/students", (req: Request, res: Response) => {
    const studentIds = students.map(student => student.id)
    res.send(studentIds)
})

server.use(handleUnknownEndpoint)

server.listen(port, () => {
    console.log("Server listening port", port)
})
