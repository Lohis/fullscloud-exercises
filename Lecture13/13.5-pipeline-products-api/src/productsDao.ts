import { executeQuery } from "./db"
import { queries } from "./queries"

export interface Product {
    id?: number
    name: string
    price: number
}

const getProducts = async () => {
    const result = await executeQuery(queries.products.getAll)
    const products: Array<Product> = result.rows
    return products
}

const getProduct = async (id: number) => {
    const queryParams = [id]
    const result = await executeQuery(queries.products.get, queryParams)
    const product: Product = result.rows[0]
    return product
}

const addProduct = async (name: string, price: number) => {
    const queryParams = [name, price]
    const result = await executeQuery(queries.products.add, queryParams)
    const id = result.rows[0].id
    const product: Product = { id, name, price }
    return product
}

const setProduct = async (id: number, name: string, price: number) => {
    const queryParams = [name, price, id]
    await executeQuery(queries.products.update, queryParams)
    const product: Product = { id, name, price }
    return product
}

const delProduct = async (id: number) => {
    const queryParams = [id]
    await executeQuery(queries.products.delete, queryParams)
}

export default {
    getProducts,
    getProduct,
    addProduct,
    setProduct,
    delProduct
}