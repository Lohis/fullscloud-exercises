
import request from "supertest"
import { jest } from "@jest/globals"
import { pool } from "../src/db/db"
import server from "../src/forum-api"

/*
Note: Testing only GET endpoints of each resource for now,
in order to prioritize deployment assignments.
*/


// eslint-disable-next-line @typescript-eslint/no-explicit-any
const initializeMockPool = (mockResponse: any) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

/*
test("Test that testing works", () => {
    expect(true).toBe(true)
})
*/

describe("Testing GET /users", () => {

    const mockResponse = {
        rows: [
            {
                "id": 1001,
                "username": "user1"
            },
            {
                "id": 1002,
                "username": "user2"
            }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("Returns 200 with all users", async () => {
        const response = await request(server)
            .get("/users")
            .set("Content-Type", "application/json")
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})

describe("Testing GET /users/:id", () => {

    const mockResponse = {
        rows: [
            {
                "id": 1001,
                "username": "user1",
                "full_name": "User User",
                "email": "user@usermail.usr"
            }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("Returns 200 with a user", async () => {
        const response = await request(server)
            .get("/users/1001")
            .set("Content-Type", "application/json")
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})

describe("Testing GET /posts", () => {

    const mockResponse = {
        rows: [
            {
                "id": 1001,
                "user_id": 111,
                "title": "Test post 1",
                "content": "The contents of test post 1.",
                "date": "2024-01-01T11:11:11.111Z"
            },
            {
                "id": 1002,
                "user_id": 222,
                "title": "Test post 2",
                "content": "The contents of test post 2.",
                "date": "2024-01-02T22:22:22.222Z"
            }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("Returns 200 with all posts", async () => {
        const response = await request(server)
            .get("/posts")
            .set("Content-Type", "application/json")
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})

describe("Testing GET /posts/:id", () => {

    const mockResponse = {
        rows: [
            {
                "id": 1001,
                "user_id": 111,
                "title": "Test post 1",
                "content": "The contents of test post 1.",
                "date": "2024-01-01T11:11:11.111Z"
            }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("Returns 200 with a post", async () => {
        const response = await request(server)
            .get("/posts/1001")
            .set("Content-Type", "application/json")
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})

describe("Testing GET /comments/:id", () => {

    const mockResponse = {
        rows: [
            {
                "content": "Comment 1 contents.",
                "date": "2024-01-01T11:11:11.111Z",
                "id": 1001,
                "post_id": 111,
                "user_id": 222
            },
            {
                "content": "Comment 2 contents.",
                "date": "2024-01-01T12:12:12.123Z",
                "id": 1002,
                "post_id": 112,
                "user_id": 222
            }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("Returns 200 with comments by userId", async () => {
        const response = await request(server)
            .get("/comments/222")
            .set("Content-Type", "application/json")
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})
