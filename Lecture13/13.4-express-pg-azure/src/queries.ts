export const queries = {
    products: {
        getAll: "SELECT * FROM products",
        get: "SELECT * FROM products WHERE id=$1",
        add: "INSERT INTO products (name, price) VALUES ($1, $2) RETURNING id",
        delete: "DELETE FROM products WHERE id=$1",
        update: "UPDATE products SET name=$1, price=$2 WHERE id=$3"
    }
} 