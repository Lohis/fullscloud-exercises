import express from "express"
import productsRouter from "./productsRouter"

const server = express()
server.use(express.json())

server.use("/products", productsRouter)

export default server