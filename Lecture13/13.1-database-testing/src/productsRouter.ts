import express, { Request, Response } from "express"
import productsDao from "./productsDao"

const router = express.Router()

router.get("/", async (req: Request, res: Response) => {
    const products = await productsDao.getProducts()
    res.send(products)
})

router.get("/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const product = await productsDao.getProduct(id)
    res.send(product)
})

router.post("/", async (req: Request, res: Response) => {
    const { name, price } = req.body
    const result = await productsDao.addProduct(name, price)
    res.send(result)
})

router.put("/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const { name, price } = req.body
    const result = await productsDao.setProduct(id, name, price)
    res.send(result)
})

router.delete("/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    await productsDao.delProduct(id)
    res.send()
})

export default router