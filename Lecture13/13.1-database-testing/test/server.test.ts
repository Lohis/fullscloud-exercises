
import request from "supertest"
import { jest } from "@jest/globals"
import { pool } from "../src/db"
import server from "../src/server"


// eslint-disable-next-line @typescript-eslint/no-explicit-any
const initializeMockPool = (mockResponse: any) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

test("Test that testing works", () => {
    expect(true).toBe(true)
})

describe("Testing GET /products", () => {

    const mockResponse = {
        rows: [
            { id: 101, name: "Test Item 1", price: 100 },
            { id: 102, name: "Test Item 2", price: 200 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("Returns 200 with all the products", async () => {
        const response = await request(server)
            .get("/products")
            .set("Content-Type", "application/json")
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })
})

describe("Testing GET /products/:id", () => {

    const mockResponse = {
        rows: [
            { id: 101, name: "Test Item 1", price: 100 }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it("Returns 200 with all the products", async () => {
        const response = await request(server)
            .get("/products/101")
            .set("Content-Type", "application/json")
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows[0])
    })
})
