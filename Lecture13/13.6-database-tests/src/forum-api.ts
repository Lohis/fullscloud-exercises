import express from "express"
import usersRouter from "./users-router"
import postsRouter from "./posts-router"
import commentsRouter from "./comments-router"
import middleware from "./middleware"

const server = express()
server.use(express.json())

server.use("/users", usersRouter)
server.use("/posts", postsRouter)
server.use("/comments", commentsRouter)
server.use(middleware.unreachableEndpoint)

export default server