import express, { Request, Response } from "express"
import usersDao from "./db/users-dao"
import postsDao from "./db/posts-dao"
import commentsDao from "./db/comments-dao"
import middleware from "./middleware"

const router = express.Router()

router.get("/", async (_req: Request, res: Response) => {
    const result = await usersDao.getAllUsers()
    res.send(result)
})

router.get("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await usersDao.getUser(id)
    res.send(result)
})

router.post("/", middleware.validateNewOrUpdateUser, async (req: Request, res: Response) => {
    const { username, fullName, email } = req.body
    const result = await usersDao.addUser(username, fullName, email)
    res.status(201).send(result)
})

router.delete("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    await commentsDao.deleteAllCommentsBelongingToUser(id)
    await postsDao.deleteAllPostsBelongingToUser(id)
    const result = await usersDao.deleteUser(id)
    res.send(result)
})

router.put("/:id", middleware.validateIdParamIsNumber, middleware.validateNewOrUpdateUser, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const { username, fullName, email } = req.body
    const result = await(usersDao.updateUser(id, username, fullName, email))
    res.send(result)
})

export default router
