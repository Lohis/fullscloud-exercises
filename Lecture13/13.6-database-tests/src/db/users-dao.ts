import { queries } from "./queries"
import executeQuery from "./db"

const getAllUsers = async () => {
    const result = await executeQuery(queries.users.getAll)
    return result.rows
}

const getUser = async (id: number) => {
    const params = [id]
    const result = await executeQuery(queries.users.get, params)
    return result.rows
}

const addUser = async (username: string, fullName: string, email: string) => {
    const params = [username, fullName, email]
    const result = await executeQuery(queries.users.add, params)
    return result.rows
}

const deleteUser = async (id: number) => {
    const params = [id]
    const result = await executeQuery(queries.users.delete, params)
    return result
}

const updateUser = async (id: number, username: string, fullName: string, email: string) => {
    const params = [id, username, fullName, email]
    const result = await executeQuery(queries.users.update, params)
    return result.rows
}

export default { getAllUsers, getUser, addUser, deleteUser, updateUser }