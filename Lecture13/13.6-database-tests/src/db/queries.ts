export const queries = {
    users: {
        getAll: "SELECT id, username FROM users;",
        get: "SELECT * FROM users WHERE id = $1;",
        add: "INSERT INTO users (username, full_name, email) VALUES ($1, $2, $3) RETURNING *;",
        delete: "DELETE FROM users WHERE id = $1 RETURNING *;",
        update: "UPDATE users SET username = $2, full_name = $3, email = $4 WHERE id = $1 RETURNING *;"
    },
    posts: {
        getAll: "SELECT id, user_id, title FROM posts;",
        get: 
            `SELECT posts.id AS post_id, posts.content AS post, posts.date AS post_date, comments.id AS comment_id, comments.content AS comment, comments.date AS comment_date 
            FROM posts JOIN comments ON posts.id = comments.post_id 
            WHERE posts.id = $1;`,

        add: "INSERT INTO posts (user_id, title, content, date) VALUES ($1, $2, $3, $4) RETURNING *;",
        delete: "DELETE FROM posts WHERE id = $1 RETURNING *;",
        deleteAllPostsByUserId: "DELETE FROM posts WHERE user_id = $1;",
        deleteAllCommentsFromPostsByUserId: "DELETE FROM comments USING posts WHERE posts.user_id = $1;",
        update: "UPDATE posts SET title = $2, content = $3 WHERE id = $1 RETURNING *;"
    },
    comments: {
        get: "SELECT * FROM comments WHERE user_id = $1;",
        add: "INSERT INTO comments (user_id, post_id, content, date) VALUES ($1, $2, $3, $4) RETURNING *;",
        delete: "DELETE FROM comments where id = $1 RETURNING *;",
        deleteAllByPostId: "DELETE FROM comments WHERE post_id = $1 RETURNING *;",
        deleteAllByUserId: "DELETE FROM comments WHERE user_id = $1 RETURNING *;",
        update: "UPDATE comments SET content = $2 WHERE id = $1 RETURNING *;"
    }
} 