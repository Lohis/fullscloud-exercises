import { queries } from "./queries"
import executeQuery from "./db"

const getAllPosts = async () => {
    const result = await executeQuery(queries.posts.getAll)
    return result.rows
}

const getPost = async (id: number) => {
    const params = [id]
    const result = await executeQuery(queries.posts.get, params)
    return result.rows
}

const addPost = async (userId: number, title: string, content: string, date: string) => {
    const params = [userId, title, content, date]
    const result = await executeQuery(queries.posts.add, params)
    return result.rows
}

const deletePost = async (commentId: number) => {
    const params = [commentId]
    const result = await executeQuery(queries.posts.delete, params)
    return result.rows
}

const deleteAllPostsBelongingToUser = async (userId: number) => {
    const params = [userId]
    await executeQuery(queries.posts.deleteAllCommentsFromPostsByUserId, params)
    const result = await executeQuery(queries.posts.deleteAllPostsByUserId, params)
    return result.rows
}

const updatePost = async (id: number, title: string, content: string) => {
    const params = [id, title, content]
    const result = await executeQuery(queries.posts.update, params)
    return result.rows
}

export default {
    getAllPosts,
    getPost,
    addPost,
    deletePost,
    deleteAllPostsBelongingToUser,
    updatePost
}