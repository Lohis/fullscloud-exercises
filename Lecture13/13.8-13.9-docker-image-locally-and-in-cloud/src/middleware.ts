import { NextFunction, Request, Response } from "express"

const unreachableEndpoint = (_req: Request, res: Response) => res.sendStatus(404)

const validateIdParamIsNumber = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    let isRequestOk = true
    if (typeof id !== "string" || id === "") isRequestOk = false
    if (Number.isNaN(Number(id)) || Number(id) < 0) isRequestOk = false
    if (!isRequestOk) return res.sendStatus(400)

    next()
}

const validateNewOrUpdateUser = (req: Request, res: Response, next: NextFunction) => {
    const { username, fullName, email } = req.body
    let isRequestOk = true
    if (typeof username !== "string" || username === "") isRequestOk = false
    if (typeof fullName !== "string" || fullName === "") isRequestOk = false
    if (typeof email !== "string" || email === "" || !email.includes("@") || !email.includes(".")) isRequestOk = false
    if (!isRequestOk) return res.sendStatus(400)

    next()
}

const validateUpdatePost = (req: Request, res: Response, next: NextFunction) => {
    const { title, content } = req.body
    let isRequestOk = true
    if (typeof title !== "string" || title == "") isRequestOk = false
    if (typeof content !== "string" || content == "") isRequestOk = false
    if (!isRequestOk) return res.sendStatus(400)

    next()
}

const validateNewPost = (req: Request, res: Response, next: NextFunction) => {
    const { userId, title, content } = req.body
    let isRequestOk = true
    if (Number.isNaN(Number(userId)) || Number(userId) < 0) isRequestOk = false
    if (typeof title !== "string" || title === "") isRequestOk = false
    if (typeof content !== "string" || content === "") isRequestOk = false
    if (!isRequestOk) return res.sendStatus(400)

    next()
}

const validateNewComment = (req: Request, res: Response, next: NextFunction) => {
    const { userId, postId, content } = req.body
    let isRequestOk = true
    if (Number.isNaN(Number(userId)) || Number(userId) < 0) isRequestOk = false
    if (Number.isNaN(Number(postId)) || Number(postId) < 0) isRequestOk = false
    if (typeof content !== "string" || content === "") isRequestOk = false
    if (!isRequestOk) return res.sendStatus(400)

    next()
}

const validateUpdateComment = (req: Request, res: Response, next: NextFunction) => {
    const { content } = req.body
    if (typeof content !== "string" || content == "") return res.sendStatus(400)

    next()
}

export default {
    unreachableEndpoint,
    validateIdParamIsNumber,
    validateNewOrUpdateUser,
    validateNewPost,
    validateUpdatePost,
    validateNewComment,
    validateUpdateComment
}