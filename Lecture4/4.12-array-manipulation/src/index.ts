const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
console.log(`Original array: ${arr}`)

const arrDivisibleByThree = arr.filter((n) => n % 3 === 0)
console.log(`Values divisible by 3: ${arrDivisibleByThree}`)

const arrMultipliedByTwo = arr.map((n) => n * 2)
console.log(`Values multiplied by 2: ${arrMultipliedByTwo}`)

const sum = arr.reduce((acc, cur) => acc += cur, 0)
console.log(`Sum of values: ${sum}`)