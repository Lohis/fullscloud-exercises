const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"]
const ordinals = ["st", "nd", "rd", "th"]

//Expects i to have been set to 0 before using this as a map function!
const mapGenPlacementDescriptions = (competitor: string) => {
    i++
    let ordinal = ordinals[3]
    if ((i).toString().endsWith("1")) ordinal = ordinals[0]
    if ((i).toString().endsWith("2")) ordinal = ordinals[1]
    if ((i).toString().endsWith("3")) ordinal = ordinals[2]

    return `${i}${ordinal} competitor was ${competitor}`
}

let i = 0 //This variable declaration outside of the map function that uses it is problematic
const placements = competitors.map(mapGenPlacementDescriptions)
console.log(placements)

//Extended competitor list for testing larger ordinal numbers like 11st, 12nd and 13rd
const competitorsExtended = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe", "Tifanie", "Cheryl", "Spiro", "Elliot", "Tien", "Ellis", "Glynn", "Shai"]
i = 0
const placementsExtended = competitorsExtended.map(mapGenPlacementDescriptions)
console.log(placementsExtended)