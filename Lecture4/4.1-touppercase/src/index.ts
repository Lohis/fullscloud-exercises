const printUppercaseFirstLetters = (str: string) => {

    const wordArr = str.split(" ")
    let output = ""

    for (let i = 0; i < wordArr.length; i++) {
        let word = wordArr[i]
        word = word[0].toUpperCase() + word.substring(1)
        output += word + " "
    }

    console.log(output.trim())
}

const str = "Myöhään yöllä höyläämön työpöydältä löytyi höyhen"
printUppercaseFirstLetters(str)