const checkIsPrimeNumber = ((num: number) => {
    for (let i = 2; i <= Math.ceil(num/2); i++) {
        if (num % i === 0) {
            return false
        }
    }
    return true
})

const integers = [2, 7, 12, 13, 33, 34, 37, 101, 1002, 46347]
const arePrimenumbers = integers.map((integer) => checkIsPrimeNumber(integer))

console.log(integers)
console.log(arePrimenumbers)