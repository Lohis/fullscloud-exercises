const words = ["city", "distribute", "battlefield", "relationship", "spread", "orchestra", "directory", "copy", "raise", "ice"]
const reverseWords = words.map((word) => word.split("").reverse().join(""))
console.log(reverseWords)