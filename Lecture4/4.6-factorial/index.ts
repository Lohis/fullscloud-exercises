const factorial = (n: number): number => {
    if (n === 0 || n === 1) return 1
    else if (n > 1) return n * factorial(n - 1)
    else return -1
}

console.log(`Factorial for negative integers returns: ${factorial(-1)}`)
console.log(`Factorial of 0: ${factorial(0)}`)
console.log(`Factorial of 1: ${factorial(1)}`)
console.log(`Factorial of 7: ${factorial(7)}`)
console.log(`Factorial of 13: ${factorial(13)}`)