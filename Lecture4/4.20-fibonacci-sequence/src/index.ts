
const fibonacci = (len: number) => {
    const arr: Array<number> = []
    if (len <= 0) return arr
    for (let i = 0; i < len; i++) {
        if (i == 0) arr.push(0)
        else if (i == 1) arr.push(1)
        else {
            arr.push(arr[i - 1] + arr[i - 2])
        }
    }
    return arr
}

console.log(`Fibonacci(8): ${fibonacci(8)}`)
console.log(`Fibonacci(0): ${fibonacci(0)}`)
console.log(`Fibonacci(1): ${fibonacci(1)}`)
console.log(`Fibonacci(-5): ${fibonacci(-5)}`) //undefined input
console.log(`Fibonacci(17): ${fibonacci(17)}`)