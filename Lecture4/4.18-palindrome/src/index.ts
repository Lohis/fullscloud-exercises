let str = process.argv[2]

//If input string is undefined, replace it with an empty string
str = str ? str : ""

//Empty string is currently evaluated as a palindrome
let isPalindrome = true
let i = 0
let j = str.length - 1
while (i < j) {
    if (str.charAt(i) !== str.charAt(j)) {
        isPalindrome = false
        break
    }
    i++
    j--
}

let output: string
if (isPalindrome) output = `Yes, ${str} is a palindrome`
else output = `No, ${str} is not a palindrome`

console.log(output)