const joinStrings = (list: Array<string>, separator: string) => {

    const joined = list.reduce((acc, cur) => {
        if (acc.length === 0) {
            return cur
        } else return acc + separator + cur
    }, "")
    
    return joined
}

const names = ["Murphy", "Hayden", "Parker", "Arden", "George", "Andie", "Ray", "Storm", "Tyler", "Pat", "Keegan", "Carroll"]
const separator = "|"

const joinedResult = joinStrings(names, separator)
console.log(joinedResult)