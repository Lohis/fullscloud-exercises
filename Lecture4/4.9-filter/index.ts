const numbers = [749385, 498654, 234534, 345467, 956876, 365457, 235667, 464534, 346436, 873453]

const filteredNumbers = numbers.filter((number) => {
    return (number % 3 === 0 || number % 5 === 0) && !(number % 3 === 0 && number % 5 === 0)
})

console.log(filteredNumbers)