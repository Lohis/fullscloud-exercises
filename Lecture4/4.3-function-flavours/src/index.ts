function sumNamed(a: number, b: number, c = 0): number {
    return a + b + c
}

const sumAnonymous = function(a: number, b: number, c = 0): number {
    return a + b + c
}

const sumArrow = (a: number, b: number, c = 0): number => a + b + c

console.log("named", sumNamed(9, 15), sumNamed(4, 8, 12))
console.log("anonymous", sumAnonymous(9, 15), sumAnonymous(4, 8, 12))
console.log("arrow", sumArrow(9, 15), sumArrow(4, 8, 12))