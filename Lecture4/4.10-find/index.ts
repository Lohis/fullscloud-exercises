const names = ["Murphy", "Hayden", "Parker", "Arden", "George", "Andie", "Ray", "Storm", "Tyler", "Pat", "Keegan", "Carroll"]

const firstMatch = names.find((name) => {
    return name.length === 3 && name.endsWith("t")
})

console.log(firstMatch)
