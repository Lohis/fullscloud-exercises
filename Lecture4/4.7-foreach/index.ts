const names = ["rauni", "matias", "Kimmo", "Heimo", "isko", "Sulevi", "Mikko", "daavid", "otso", "herkko"]
names.forEach((name) => {
    const initialLetter = name[0].toUpperCase()
    const theRest = name.substring(1)
    console.log(initialLetter + theRest)
})