const generateRandomInt = (min: number, max: number) => {
    const num = Math.floor(Math.random() * (max - min)) + min
    return Math.round(num)
}

const results = [0, 0, 0, 0, 0, 0]
let output = ""
for (let i = 0; i < 100; i++) {
    const num = generateRandomInt(1, 7)
    results[num-1]++
    output += generateRandomInt(1, 7) + " "
}

console.log(output)
console.log(results)