
const calculateBigSum = (callback: (n: number) => void) => {
    let sum = 0
    for (let i = 1; i < 1_000_000_000; i ++) {
        if (i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
            sum += i
        }
    }
    callback(sum)
}

calculateBigSum((sum) => console.log(sum))
