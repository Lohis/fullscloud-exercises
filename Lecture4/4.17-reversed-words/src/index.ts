let input = process.argv[2]

//Replace undefined input argument with an empty string
input = input ? input : ""

const strArr = input.split(" ")
console.log(strArr)

const strArrFlipped = strArr.map((str) => [...str].reverse().join(""))

console.log(strArrFlipped)

const output = strArrFlipped.reduce((acc, cur) => {
    if (acc.length === 0) return cur
    else return `${acc} ${cur}`
}, "")

console.log(output)