const generateDice = (diceSides: number = 6) => {
    return () => {
        const min = 1
        const max = diceSides + 1
        const num = Math.floor(Math.random() * (max - min)) + min
        return Math.round(num)
    }
}

const sixSideDice = generateDice(6)
const eightSideDice = generateDice(8)

const damage = sixSideDice() + eightSideDice()
console.log(`Damage dealt: ${damage}`)

/*
let output = "testing d6: "
for (let i = 0; i < 100; i++) {
    output += sixSideDice() + " "
}
console.log(output)

output = "testing d8: "
for (let i = 0; i < 100; i++) {
    output += eightSideDice() + " "
}
console.log(output)
*/
