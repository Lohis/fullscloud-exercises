const charIndex  = {
    a: 1,
    b: 2,
    c: 3,
    d: 4,
    e: 5,
    f: 6,
    g: 7,
    h: 8,
    i: 9,
    j: 10,
    k: 11,
    l: 12,
    m: 13,
    n: 14,
    o: 15,
    p: 16,
    q: 17,
    r: 18,
    s: 19,
    t: 20,
    u: 21,
    v: 22,
    w: 23,
    x: 24,
    y: 25,
    z: 26
}

//For enabling a dictionary-style access to charIndex properties, create a custom key type
type charIndexKey = keyof typeof charIndex 

const word1 = "Raspberry"
const word2 = "Pi"
const word3 = "bead"
const word4 = "rose"

const charIndexify = (word: string): string => {
    const chars = [...word.toLowerCase()] //effectively same as word.toLowerCase().split("")
    const indexArr = chars.map((char) => {
        const key = char as charIndexKey //use char as a key value, using the custom key type
        return charIndex[key]
    })
    
    const output = indexArr.reduce((acc, cur) => {
        return acc += cur
    }, "")

    return output
}

console.log(`${word1} becomes ${charIndexify(word1)}`)
console.log(`${word2} becomes ${charIndexify(word2)}`)
console.log(`${word3} becomes ${charIndexify(word3)}`)
console.log(`${word4} becomes ${charIndexify(word4)}`)