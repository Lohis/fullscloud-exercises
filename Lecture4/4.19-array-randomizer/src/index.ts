const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32]

const generateRandomInt = (min: number, max: number) => {
    const num = Math.floor(Math.random() * (max - min)) + min
    return Math.round(num)
}

for (let i = 0; i < array.length; i++) {
    const randIndex = generateRandomInt(0, array.length - 1)
    const temp = array[randIndex]
    array[randIndex] = array[i]
    array[i] = temp
}

console.log(array)