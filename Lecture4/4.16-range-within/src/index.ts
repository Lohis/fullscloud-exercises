let start = Number.parseInt(process.argv[2])
let end = Number.parseInt(process.argv[3])

//Sanitize arguments, use 0 as default value
start = start ? start : 0
end = end ? end : 0

const isAscending = start < end
const increment: number = isAscending ? 1 : -1

const numArr: Array<number> = []
for (let i = start; i != end; i += increment) {
    numArr.push(i)
}
numArr.push(end)

console.log(`Array size: ${numArr.length}`)
console.log(`Values: ${numArr}`)
