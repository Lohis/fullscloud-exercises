import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";
import axios from "axios"

let counter = 0

const generateRandomInt = (min: number, max: number) => Math.floor(Math.random() * (max - min) + min)
const randomDogUrl = "https://dog.ceo/api/breeds/image/random"
const randomCatUrl = "https://cataas.com/cat?json=true"
const randomFoxUrl =  "https://randomfox.ca/floof"

const catServiceUrl = "https://cataas.com/cat/"

export async function sl00010CatsAndDogs(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    let responseJSON = {
        "message": "",
        "link": ""
    }

    if (++counter % 13 !== 0) {
        if (generateRandomInt(0, 2)) {
            const result = (await axios.get(randomDogUrl)).data
            responseJSON.message = "You got a dog!"
            responseJSON.link = result["message"]
        } else {
            const result = (await axios.get(randomCatUrl)).data
            responseJSON.message = "You got a cat!"
            responseJSON.link = `${catServiceUrl}${result["_id"]}`
        }
    } else {
        const result = (await axios.get(randomFoxUrl)).data
        responseJSON.message = "You got a fox!"
        responseJSON.link = result["link"]
    }

    return { body: `${JSON.stringify(responseJSON)}` };
};

app.http('sl00010CatsAndDogs', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: sl00010CatsAndDogs
});
