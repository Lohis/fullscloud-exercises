import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";

export async function sl00071AzureFunctionProject(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    const input = request.query.get('input');

    return { body: `${input.toUpperCase()}` };
};

app.http('sl00071AzureFunctionProject', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: sl00071AzureFunctionProject
});
