import { app, HttpRequest, HttpResponseInit, InvocationContext } from "@azure/functions";

const generateRandomNumber = (min: number, max: number) => Math.random() * (max-min) + min

export async function sl00079RandomNumberFunction(request: HttpRequest, context: InvocationContext): Promise<HttpResponseInit> {
    context.log(`Http function processed request for url "${request.url}"`);

    const min = Number.parseFloat(request.query.get('min')) || 0;
    const max = Number.parseFloat(request.query.get('max')) || 2;
    const useInt = Number.parseFloat(request.query.get('useint')) || 0 ;
    
    let randomNum = generateRandomNumber(min, max)
    if (useInt) randomNum = Math.floor(randomNum)

    return {body: `${randomNum}`};
};

app.http('sl00079RandomNumberFunction', {
    methods: ['GET', 'POST'],
    authLevel: 'anonymous',
    handler: sl00079RandomNumberFunction
});
