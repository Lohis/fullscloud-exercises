import { convert } from "../src/converter"

describe("Test converter", () => {
    it("0 amount always returns 0 regardless of specified units", () => {
        expect(convert(0, "any source unit", "any target unit"))
    })

    it("Same source and destination units return the original amount", () => {
        expect(convert(45.678, "any source unit", "any source unit"))
    })

    it("converting from liter works", () => {
        expect(convert(12.345, "l", "l")).toBeCloseTo(12.345)
        expect(convert(12.345, "l", "dl")).toBeCloseTo(123.45)
        expect(convert(12.345, "l", "oz")).toBeCloseTo(434.4833)
        expect(convert(12.345, "l", "cup")).toBeCloseTo(43.44833)
        expect(convert(12.345, "l", "pint")).toBeCloseTo(21.72416)
    })
    
    it("converting from deciliter works", () => {
        expect(convert(12.345, "dl", "l")).toBeCloseTo(1.2345)
        expect(convert(12.345, "dl", "dl")).toBeCloseTo(12.345)
        expect(convert(12.345, "dl", "oz")).toBeCloseTo(43.44833)
        expect(convert(12.345, "dl", "cup")).toBeCloseTo(4.344833)
        expect(convert(12.345, "dl", "pint")).toBeCloseTo(2.172416)
    })

    it("converting from ounce works", () => {
        expect(convert(12.345, "oz", "l")).toBeCloseTo(0.3507592)
        expect(convert(12.345, "oz", "dl")).toBeCloseTo(3.507592)
        expect(convert(12.345, "oz", "oz")).toBeCloseTo(12.345)
        expect(convert(12.345, "oz", "cup")).toBeCloseTo(1.2345)
        expect(convert(12.345, "oz", "pint")).toBeCloseTo(0.61725)
    })

    it("converting from cup works", () => {
        expect(convert(12.345, "cup", "l")).toBeCloseTo(3.507592)
        expect(convert(12.345, "cup", "dl")).toBeCloseTo(35.07592)
        expect(convert(12.345, "cup", "oz")).toBeCloseTo(123.45)
        expect(convert(12.345, "cup", "cup")).toBeCloseTo(12.345)
        expect(convert(12.345, "cup", "pint")).toBeCloseTo(6.1725)
    })

    it("converting from pint works", () => {
        expect(convert(12.345, "pint", "l")).toBeCloseTo(7.015185)
        expect(convert(12.345, "pint", "dl")).toBeCloseTo(70.15185)
        expect(convert(12.345, "pint", "oz")).toBeCloseTo(246.9)
        expect(convert(12.345, "pint", "cup")).toBeCloseTo(24.69)
        expect(convert(12.345, "pint", "pint")).toBeCloseTo(12.345)
    })
})
