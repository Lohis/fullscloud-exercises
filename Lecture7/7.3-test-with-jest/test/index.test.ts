import { calculator } from "../src/index"

/*
test("dummy test" , () => {
    expect(true).toBe(true)
})
*/

test("5 times 3 should be 15", () => {
    expect(calculator("*", 3, 5)).toBe(15)
})

test("Value multiplied with 0 should be 0", () => {
    expect(calculator("*", 0, 5)).toBe(0)
})

test("Negative value multiplied with positive value should return negative value", () => {
    expect(calculator("*", -3, 5)).toBe(-15)
})

test("Two negative values multiplied with each other should return positive value", () => {
    expect(calculator("*", -3, -5)).toBe(15)
})

test("NaN should return NaN", () => {
    expect(calculator("*", NaN, -5)).toBe(NaN)
})
