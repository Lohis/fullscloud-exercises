import { calculator } from "../src/index"

/*
test("dummy test" , () => {
    expect(true).toBe(true)
})
*/

test("5 times 3 should be 15", () => {
    expect(calculator("*", 3, 5)).toBe(15)
})

test("Value multiplied with 0 should be 0", () => {
    expect(calculator("*", 0, 5)).toBe(0)
})

test("Negative value multiplied with positive value should return negative value", () => {
    expect(calculator("*", -3, 5)).toBe(-15)
})

test("Two negative values multiplied with each other should return positive value", () => {
    expect(calculator("*", -3, -5)).toBe(15)
})

test("NaN should return NaN", () => {
    expect(calculator("*", NaN, -5)).toBe(NaN)
})

describe("Calculator division", () => {
    it("Basic division should work", () => {
        expect(calculator("/", 6, 3)).toBe(2)
        expect(calculator("/", 6, -3)).toBe(-2)
    })

    it("0 divided by any value should be 0", () => {
        expect(Math.abs(calculator("/", 0, 5))).toBe(0)
        expect(Math.abs(calculator("/", 0, -46465.3535))).toBe(0)
    })
    
    it("Fraction results should work", () => {
        expect(calculator("/", 4, 3)).toBeCloseTo(1.333333333333)
        expect(calculator("/", 1, 2)).toBeCloseTo(0.5)
    })
    
    it("Any value divided by zero should be Infinity", () => {
        expect(calculator("/", 235, 0)).toBe(Infinity)
    })
})