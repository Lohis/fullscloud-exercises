import fs from "fs"

export const convert = (amount: number, sourceUnit: string, targetUnit: string): number => {
    if (amount === 0) return 0
    if (sourceUnit === targetUnit) return amount

    const units = JSON.parse(fs.readFileSync("./data/units.json", "utf-8"))
    if (sourceUnit === undefined || targetUnit === undefined) return -1 //this should rather throw an error

    return amount * units[sourceUnit] / units[targetUnit]
}