import { convert } from "./converter"

const amount = Number.parseFloat(process.argv[2])
const sourceUnit = process.argv[3]
const targetUnit = process.argv[4]

console.log(convert(amount, sourceUnit, targetUnit))