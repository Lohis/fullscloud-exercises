import { executeQuery } from "./db"
import product from "./product"
import { queries } from "./queries"

export const getAllProducts = async () => {
    const result = await executeQuery(queries.getAllQuery)
    return result.rows
}

export const getProduct = async (id: number) => {
    const result = await executeQuery(queries.getQuery, [id])
    return result.rows
}

export const insertProduct = async (product: product) => {
    const params = [product.name, product.price]
    const result = await executeQuery(queries.insertQuery, params)
    return result.rows
}

export const deleteProduct = async (id: number) => {
    const result = await executeQuery(queries.deleteQuery, [id])
    return result.rows
}

export const updateProduct = async (id: number, product: product) => {
    const params = [id, product.name, product.price]
    const result = await executeQuery(queries.updateQuery, params)
    return result.rows
}