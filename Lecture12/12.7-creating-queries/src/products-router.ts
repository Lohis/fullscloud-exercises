import express, { Request, Response } from "express"
import { validateProductHasRequiredFields } from "./middleware"
import { getAllProducts, getProduct, insertProduct, updateProduct, deleteProduct } from "./products-dao"
import product from "./product"

const router = express.Router()

//read all products
router.get("/", async (req: Request, res: Response) => {
    const result = await getAllProducts()
    res.send(result)
})

//read single product
router.get("/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await getProduct(id)
    res.send(result)
})

//create product
router.post("/", validateProductHasRequiredFields, async (req: Request, res: Response) => {
    const product = new Object as product
    const { name, price } = req.body
    product.name = name
    product.price = price
    const result = await insertProduct(product)
    res.send(result)
})

//update product
router.put("/:id", validateProductHasRequiredFields, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const product = new Object as product
    const { name, price } = req.body
    product.name = name
    product.price = price
    const result = await updateProduct(id, product)
    res.send(result)
})

//delete product
router.delete("/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await deleteProduct(id)
    res.send(result)
})

export default router