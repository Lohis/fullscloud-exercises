
export const queries = {
    getAllQuery: "SELECT * FROM products;",
    getQuery: "SELECT * FROM products WHERE id = $1;",
    insertQuery: "INSERT INTO products (name, price) VALUES ($1, $2) RETURNING *;",
    updateQuery: "UPDATE products SET name = $2, price = $3 WHERE id = $1 RETURNING *",
    deleteQuery: "DELETE FROM products WHERE id = $1 RETURNING *;"
}