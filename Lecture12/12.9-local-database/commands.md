Create container (terminal) :

`docker run --name forum-pg -e POSTGRES_PASSWORD=pg-pass -e POSTGRES_USER=pg-user -p 5432:5432 -d postgres:16.1`

Log in (terminal) :

`docker exec -it forum-pg psql -U pg-user`

Create new database "forum" (psql) :

`CREATE DATABASE forum;`

Connect to "forum" database (psql) :

`\c forum`

