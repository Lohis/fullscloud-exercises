import express from "express"
import { createProductsTable } from "./db"
import productsRouter from "./products-router"
import { logRequest, unreachableEndpointResponse } from "./middleware"

const server = express()
server.use(express.json())

createProductsTable()

server.use(logRequest)
server.use("/products", productsRouter)
server.use(unreachableEndpointResponse)

const { PORT } = process.env
server.listen(PORT, () => {
    console.log("Products API listening to port", PORT)
})