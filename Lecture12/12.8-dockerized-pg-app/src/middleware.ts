import { Request, Response, NextFunction } from "express"

export const logRequest = (req: Request, res: Response, next: NextFunction) => {
    const url = req.url
    const reqTime = new Date().toISOString()

    console.log(`API received request at: ${reqTime}
        url: ${url}
        --------------------`)
    
    next()
}

export const validateProductHasRequiredFields = (req: Request, res: Response, next: NextFunction) => {
    const { name, price } = req.body
    let isRequestOk = true
    if (typeof name !== "string" || name === "") isRequestOk = false
    if (typeof price !== "string" || price === "") isRequestOk = false
    if (Number.isNaN(Number(price))) isRequestOk = false
    if (!isRequestOk) return res.status(400).send("Invalid or missing product fields")

    next()
}

export const unreachableEndpointResponse = (_req: Request, res: Response) => res.sendStatus(404)