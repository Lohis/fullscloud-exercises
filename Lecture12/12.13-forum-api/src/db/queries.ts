export const queries = {
    users: {
        getAll: "SELECT id, username FROM users;",
        get: "SELECT * FROM users WHERE id = $1;",
        add: "INSERT INTO users (username, full_name, email) VALUES ($1, $2, $3) RETURNING *;"
    },
    posts: {
        getAll: "SELECT id, user_id, title FROM posts;",

        get: `SELECT posts.id AS post_id, posts.content AS post, posts.date AS post_date, comments.id AS comment_id, comments.content AS comment, comments.date AS comment_date 
                FROM posts JOIN comments ON posts.id = comments.post_id 
                WHERE posts.id = $1;`,
                
        add: "INSERT INTO posts (user_id, title, content, date) VALUES ($1, $2, $3, $4) RETURNING *;"
    },
    comments: {
        get: "SELECT * FROM comments WHERE user_id = $1;",
        add: "INSERT INTO comments (user_id, post_id, content, date) VALUES ($1, $2, $3, $4) RETURNING *;"
    }
} 