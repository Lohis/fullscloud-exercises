import express, { Request, Response } from "express"
import dao from "./db/comments-dao"
import middleware from "./middleware"

const router = express.Router()

router.get("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await dao.getComment(id)
    res.send(result)
})

router.post("/", middleware.validateNewComment, async (req: Request, res: Response) => {
    const { userId, postId, content } = req.body
    const date = new Date().toISOString()
    const result = await dao.addComment(userId, postId, content, date)
    res.status(201).send(result)
})

export default router