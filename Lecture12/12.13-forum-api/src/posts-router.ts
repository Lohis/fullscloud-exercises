import express, { Request, Response } from "express"
import dao from "./db/posts-dao"
import middleware from "./middleware"

const router = express.Router()

router.get("/", async (req: Request, res: Response) => {
    const result = await dao.getAllPosts()
    res.send(result)
})

router.get("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await dao.getPost(id)
    res.send(result)
})

router.post("/", middleware.validateNewPost, async (req: Request, res: Response) => {
    const { userId, title, content } = req.body
    const date = new Date().toISOString()
    const result = await dao.addPost(userId, title, content, date)
    res.status(201).send(result)
})

export default router