Commands for inserting content in the tables

```
INSERT INTO users (username, full_name, email)
VALUES
('roope', 'Roope Ankka', 'johtaja@ra-yhtyma.ra'),
('aku13', 'Aku Ankka', 'aankka@roopemail.ra'),
('11nes', 'Iines Ankka', '11nes@ankkalinna.ank'),
('pelle', 'Pelle Peloton', 'pelle@pelotonpaja.ank'),
('taavi', 'Taavi Ankka', 'taavi.ankka@ankkalinnanyliopisto.ank')
;
```

```
INSERT INTO posts (user_id, title, content, date)
VALUES
(4, 'Heureka!', 'Olen paikallistanut telemakrospektroskoopillani plenetoidin joka on täyttä kultaa.', '2024-01-01'),
(2, 'Matkoilla taas', 'Tällä kertaa avaruudessa keräämässä lisää kultaa sedälle. Onneksi muut velkojani eivät yletä avaruuteen', '2024-01-05'),
(5, 'Väitökseni kantaa hedelmää', 'Väitöskirjani avaruusspektrografiasta kantaa hienosti hedelmää Pelle Pelottoman soveltamana.', '2024-01-06'),
(3, 'Kampausnäytös', 'Ensi tiistain kampausnäytöksestä on tulossa upea.', '2024-01-03'),
(2, 'Hyppypillerit', 'Harjoittelen avaruuden painottomuutta Pellen hyppypillereillä ja menetin miltei koko painoni', '2024-01-06')
;
```

```
INSERT INTO comments (user_id, post_id, content, date)
VALUES
(2, 1, 'Älä kerro asiasta Roopelle, tai hän pakottaa minut avaruuteen vuokrarästeihini vedoten', '2024-01-02'),
(1, 1, 'Myöhäistä, Akulainen. Laukaisu on loppiaisen jälkeisenä maanantaina. Äläkä yritä livistää!', '2024-01-03'),
(5, 1, 'Onpa kiintoisaa! Mikähän on saanut aikaan noin homogeenisen koostumuksen?', '2024-01-06'),
(3, 2, 'Muista että olet luvannut viedä minut tiistaina kampausnäytökseen. Pidäkin huoli siitä että palaat ajoissa takaisin!', '2024-01-07'),
(4, 5, 'Muista ottaa vain yksi hyppypilleri kerrallaan, muuten menetät liikaa painoa.', '2024-01-07')
;
``