Commands

```
CREATE TABLE users (
id SERIAL PRIMARY KEY,
username VARCHAR NOT NULL UNIQUE,
full_name VARCHAR NOT NULL,
email VARCHAR NOT NULL UNIQUE
);
```

```
CREATE TABLE posts (
id SERIAL PRIMARY KEY,
user_id INT NOT NULL REFERENCES users(id),
title VARCHAR NOT NULL,
content VARCHAR NOT NULL,
date DATE NOT NULL
);
```

```
CREATE TABLE comments (
id SERIAL PRIMARY KEY,
user_id INT NOT NULL REFERENCES users(id),
post_id INT NOT NULL REFERENCES posts(id),
content VARCHAR NOT NULL,
date DATE NOT NULL
);
```