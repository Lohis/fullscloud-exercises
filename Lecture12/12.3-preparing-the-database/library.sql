CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "full_name" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL,
  "created_at" timestamp NOT NULL
);

CREATE TABLE "genres" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar UNIQUE NOT NULL
);

CREATE TABLE "languages" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar UNIQUE NOT NULL
);

CREATE TABLE "books" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar NOT NULL,
  "release_year" int NOT NULL,
  "genre_id" int NOT NULL,
  "language_id" int NOT NULL,
  CONSTRAINT fk_genre FOREIGN KEY (genre_id) REFERENCES genres(id),
  CONSTRAINT fk_language FOREIGN KEY (language_id) REFERENCES languages(id)
);

CREATE TABLE "loans" (
  "id" SERIAL PRIMARY KEY,
  "book_id" INT NOT NULL,
  "user_id" INT NOT NULL,
  "loan_date" timestamp NOT NULL,
  "due_date" timestamp NOT NULL,
  "return_date" timestamp,
  CONSTRAINT fk_book FOREIGN KEY (book_id) REFERENCES books(id),
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO users (full_name, email, created_at)
VALUES 
    ('Teppo Testaaja', 'teppo.testaaja@buutti.com', NOW()),
    ('Taija Testaaja', 'taija.testaaja@buutti.com', NOW()),
    ('Outi Ohjelmoija', 'outi.ohjelmoija@buutti.com', NOW()),
    ('Olli Ohjelmoija', 'olli.ohjelmoija@buutti.com', NOW());

INSERT INTO genres (name)
VALUES 
    ('Scifi'),
    ('Fantasy'),
    ('Comic book'),
    ('Horror'),
    ('Drama');

INSERT INTO languages (name)
VALUES
    ('Finnish'),
    ('English'),
    ('Swedish');

INSERT INTO books (name, release_year, genre_id, language_id)
VALUES
    ('Taru Sormusten Herrasta', 1954, 2, 1),
    ('Silmarillion', 1977, 2, 2),
    ('The Hitchhikers Guide to the Galaxy', 1979, 1, 2),
	('The Walking Dead: Days Gone Bye', 2008, 3, 2);

INSERT INTO loans (book_id, user_id, loan_date, due_date, return_date)
VALUES
    (1, 1, '2000-01-01', '2000-01-15', '2000-01-15'),
    (1, 2, '2000-02-12', '2000-02-26', null),
    (2, 2, '2000-02-12', '2000-02-26', null),
    (3, 1, '2000-03-03', '2000-03-17', '2000-03-05'),
    (4, 3, '2000-03-04', '2000-03-18', '2000-03-08'),
    (4, 4, '2000-03-08', '2000-03-22', null);