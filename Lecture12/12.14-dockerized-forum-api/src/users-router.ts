import express, { Request, Response } from "express"
import dao from "./db/users-dao"
import middleware from "./middleware"

const router = express.Router()

router.get("/", async (_req: Request, res: Response) => {
    const result = await dao.getAllUsers()
    res.send(result)
})

router.get("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await dao.getUser(id)
    res.send(result)
})

router.post("/", middleware.validateNewUser, async (req: Request, res: Response) => {
    const { username, fullName, email } = req.body
    const result = await dao.addUser(username, fullName, email)
    res.status(201).send(result)
})

export default router
