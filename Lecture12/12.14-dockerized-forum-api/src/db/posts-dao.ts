import { queries } from "./queries"
import executeQuery from "./db"

const getAllPosts = async () => {
    const result = await executeQuery(queries.posts.getAll)
    return result.rows
}

const getPost = async (id: number) => {
    const params = [id]
    const result = await executeQuery(queries.posts.get, params)
    return result.rows
}

const addPost = async (userId: number, title: string, content: string, date: string) => {
    const params = [userId, title, content, date]
    const result = await executeQuery(queries.posts.add, params)
    return result.rows
}

export default { getAllPosts, getPost, addPost }