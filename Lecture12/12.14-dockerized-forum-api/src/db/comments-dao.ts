import { queries } from "./queries"
import executeQuery from "./db"

const getComment = async (userId: number) => {
    const params = [userId]
    const result = await executeQuery(queries.comments.get, params)
    return result.rows
}

const addComment = async (userId: number, postId: number, content: string, date: string) => {    
    const params = [userId, postId, content, date]
    const result = await executeQuery(queries.comments.add, params)
    return result.rows
}

export default { getComment, addComment }