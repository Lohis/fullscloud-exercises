import server from "./forum-api"

const isValidEnv = (): boolean => {
    const { PORT, PG_HOST, PG_PORT, PG_USER, PG_PASSWORD, PG_DATABASE, NODE_ENV } = process.env
    
    if (typeof PORT !== "string" || PORT === "") return false
    if (typeof PG_HOST !== "string" || PG_HOST === "") return false
    if (typeof PG_PORT !== "string" || PG_PORT === "") return false
    if (typeof PG_USER !== "string" || PG_USER === "") return false
    if (typeof PG_PASSWORD !== "string" || PG_PASSWORD === "") return false
    if (typeof PG_DATABASE !== "string" || PG_DATABASE === "") return false
    if (typeof PG_DATABASE !== "string" || PG_DATABASE === "") return false
    if (typeof NODE_ENV !== "string" || NODE_ENV === "") return false
    
    return true
}

if (!isValidEnv()) {
    console.log("Error: no required environment variables found")
    process.exit(1)
}

const PORT = process.env.PORT
server.listen(PORT, () => console.log("Listening to port", PORT))