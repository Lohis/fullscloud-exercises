Commands for updating one email address and deleting a user.

First creating a new user for this exercise

```
INSERT INTO users (username, full_name, email)
VALUES ('sudenpentu123', 'Tupu Ankka', 'tupuank@roopemail.ra')
;
```


Then doing the modification and deletion

```
UPDATE users
SET email = 'tupuankka@roopemail.ra'
WHERE full_name = 'Tupu Ankka'
;
```

```
DELETE FROM users
WHERE full_name = 'Tupu Ankka'
;
```

Note: the above selection by full_name is very dangerous as that field does not require an unique value.