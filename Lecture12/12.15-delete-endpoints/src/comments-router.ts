import express, { Request, Response } from "express"
import commentsDao from "./db/comments-dao"
import middleware from "./middleware"

const router = express.Router()

router.get("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await commentsDao.getComment(id)
    res.send(result)
})

router.post("/", middleware.validateNewComment, async (req: Request, res: Response) => {
    const { userId, postId, content } = req.body
    const date = new Date().toISOString()
    const result = await commentsDao.addComment(userId, postId, content, date)
    res.status(201).send(result)
})

router.delete("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await commentsDao.deleteComment(id)
    res.send(result)
})

export default router