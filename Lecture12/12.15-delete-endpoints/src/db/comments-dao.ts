import { queries } from "./queries"
import executeQuery from "./db"

const getComment = async (userId: number) => {
    const params = [userId]
    const result = await executeQuery(queries.comments.get, params)
    return result.rows
}

const addComment = async (userId: number, postId: number, content: string, date: string) => {    
    const params = [userId, postId, content, date]
    const result = await executeQuery(queries.comments.add, params)
    return result.rows
}

const deleteComment = async (commentId: number) => {
    const params = [commentId]
    const result = await executeQuery(queries.comments.delete, params)
    return result.rows
}

const deleteAllCommentsBelongingToPost = async (postId: number) => {
    const params = [postId]
    const result = await executeQuery(queries.comments.deleteAllByPostId, params)
    return result.rows
}

const deleteAllCommentsBelongingToUser = async (userId: number) => {
    const params = [userId]
    const result = await executeQuery(queries.comments.deleteAllByUserId, params)
    return result.rows
}

export default { 
    getComment,
    addComment,
    deleteComment,
    deleteAllCommentsBelongingToPost,
    deleteAllCommentsBelongingToUser
}