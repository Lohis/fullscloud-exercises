import express, { Request, Response } from "express"
import postsDao from "./db/posts-dao"
import commentsDao from "./db/comments-dao"
import middleware from "./middleware"

const router = express.Router()

router.get("/", async (req: Request, res: Response) => {
    const result = await postsDao.getAllPosts()
    res.send(result)
})

router.get("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const result = await postsDao.getPost(id)
    res.send(result)
})

router.post("/", middleware.validateNewPost, async (req: Request, res: Response) => {
    const { userId, title, content } = req.body
    const date = new Date().toISOString()
    const result = await postsDao.addPost(userId, title, content, date)
    res.status(201).send(result)
})

router.delete("/:id", middleware.validateIdParamIsNumber, async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    await commentsDao.deleteAllCommentsBelongingToPost(id)
    const result = await postsDao.deletePost(id)
    res.send(result)
})

export default router