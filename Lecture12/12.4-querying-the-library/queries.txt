1.
SELECT * FROM loans WHERE loan_date < '2000-03-01';

2.
SELECT * FROM loans WHERE return_date IS NOT null;

3.
SELECT users.full_name, loans.loan_date 
FROM users JOIN loans 
ON users.id = loans.user_id 
WHERE users.id = 1
;

4.
SELECT books.name, books.release_year, languages.name 
FROM books JOIN languages 
ON books.language_id = languages.id 
WHERE release_year > 1960
;


