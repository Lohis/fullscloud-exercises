const price = 34.95
const discountPercentage = 30

const roundToTwoDecimals = (num) => Math.round(num * 100) / 100

//discounted price is rounded to two decimals, more than that doesn't make sense with monetary values
const discountedPrice = roundToTwoDecimals(price * (100 - discountPercentage) / 100)

//presenting the prices with 2 decimals, even if the Number variables don't have any
console.log("original price: " + price.toFixed(2))
console.log("discount percentage: " + discountPercentage)
console.log("discounted price: " + discountedPrice.toFixed(2))