const distance = 55
const speed = 47.8

const prettyPrintHours = (hours) => `${Math.trunc(hours)}h ${(Math.round(hours % 1 * 60))}min`

const travelTime = distance / speed
console.log(prettyPrintHours(travelTime))
