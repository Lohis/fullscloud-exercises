const numberOfPeople = process.argv[2]
const groupSize = process.argv[3]

const numberOfGroups = Math.ceil(numberOfPeople / groupSize)

console.log("Number of groups: " + numberOfGroups)