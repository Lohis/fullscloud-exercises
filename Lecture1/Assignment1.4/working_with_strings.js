const str1 = "kissa"
const str2 = "kana"

const combined = str1 + str2

console.log("str1 length: " + str1.length)
console.log("str2 length: " + str2.length)
console.log("combined length: " + combined.length)

const avgLength = (str1.length + str2.length) / 2
console.log("avg length: " + avgLength)

console.log(str1.toUpperCase())

const first_letter = str2[0]
const last_letter = str2[str2.length-1]
console.log(`${first_letter} ${last_letter}`)
