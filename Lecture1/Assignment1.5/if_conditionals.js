const playerCount = 4
if (playerCount === 4) {
    console.log("Hearts can be played")
}

const isStressed = false
const hasIcecream = true
if (!isStressed && hasIcecream) {
    console.log("Mark is happy")
}

const isSunny = true
const isRainy = false
const temperature = 20
if (isSunny && !isRainy && temperature >= 20) {
    console.log("It's a beach day")
}

const seesSuzy = true;
const seesDan = false;
const isTuesdayNight = true;
if ((isTuesdayNight && seesSuzy && !seesDan) || (isTuesdayNight && !seesSuzy && seesDan)) {
    console.log("Arin is happy")
} else {
    console.log("Arin is not happy")
}