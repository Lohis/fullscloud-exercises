const daysInYear = 365
const hoursInDay = 24
const secondsInHour = 60 * 60

const secondsInYear = secondsInHour * hoursInDay * daysInYear

console.log("Seconds in a year: " + secondsInYear)