const lastName = "Korhonen"
const age = 42
const isDoctor = false
const sender = "Juhani Virtanen"

const addOrdinalIndicator = (num) => {
    if (typeof(num) === "number") {
        let numStr = num.toString()
        if (numStr.endsWith("1")) {
            numStr = numStr + "st"
        } else if (numStr.endsWith("2")) {
            numStr = numStr + "nd"
        } else if (numStr.endsWith("3")) {
            numStr = numStr + "rd"
        } else {
            numStr = numStr + "th"
        }
        return numStr
    } else return num
}

const title = isDoctor ? "Dr." : "Mx."
const nextAge = age + 1

const message = `Dear ${title} ${lastName}

Congratulations on your ${addOrdinalIndicator(nextAge)} birthday! Many happy returns!

Sincerely,
${sender}

`

console.log(message)