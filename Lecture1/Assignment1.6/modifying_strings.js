const argumentString = process.argv[2]
console.log(argumentString)

let modifiedString = argumentString.trim()
console.log(modifiedString)

const maxLength = 20
if (modifiedString.length > maxLength) {
    modifiedString = modifiedString.substring(0, 20)
}
console.log(modifiedString)

modifiedString = modifiedString[0].toLowerCase() + modifiedString.substring(1)
console.log(modifiedString)