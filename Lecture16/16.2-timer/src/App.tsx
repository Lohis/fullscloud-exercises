import { useEffect, useState } from 'react'

const App = () => {
    const [seconds, setSeconds] = useState(0)
    const [isRunning, setIsRunning] = useState(false)

    const addSeconds = (amount: number) => {
        setSeconds(seconds + amount)
    }

    const formatTimerValue = (secs: number) => {
        let mm = String(Math.floor(secs / 60))
        mm = mm.length < 2 ? "0" + mm : mm

        let ss = String(secs % 60)
        ss = ss.length < 2 ? "0" + ss : ss

        return `${mm}:${ss}`
    }

    useEffect(() => { 
        if (isRunning) {
            const tick = setTimeout(() => addSeconds(1), 1000)
            return () => clearTimeout(tick)
        }
    }, [seconds, isRunning])

    return (
        <>
            <h1>Timer</h1>
            <div>{formatTimerValue(seconds)}</div>
            <div>
                <button onClick={() => {addSeconds(1)}}>+1 sec</button>
                <button onClick={() => {addSeconds(60)}}>+1 min</button>
            </div>
            <div>
                <button onClick={() => {setIsRunning(true)}}>Start</button>
                <button onClick={() => {setIsRunning(false)}}>Stop</button>
            </div>
        </>
    )
}

export default App
