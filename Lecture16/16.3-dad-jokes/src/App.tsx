import { useEffect, useState } from 'react'
import axios from "axios"

const App = () => {
    const [joke, setJoke] = useState("")

    useEffect(() => {
        const fetchData = async () => {
            const url = 'https://api.api-ninjas.com/v1/dadjokes'
            const config = {
                headers: {
                    'X-Api-Key': 'qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM'
                }
            }
            const response = await axios.get(url, config)
            const newJoke = response.data[0]["joke"]
            setJoke(newJoke)
        }
        fetchData()
    }, [])

    return (
        <>
            <h1>Dad jokes</h1>
            <p>{joke}</p>
        </>
    )
}

export default App
