import { ChangeEvent, useState } from "react"

interface InputFieldProps {
    addPhoneNumber: (value: string) => void
}

const InputField = (props: InputFieldProps) => {
    const { addPhoneNumber } = props
    
    const [phoneNumber, setPhoneNumber] = useState("")

    const setNumber = (event: ChangeEvent<HTMLInputElement>) => {
        const newPhoneNumber = String(event.target.value)
        const latestChar = newPhoneNumber.charAt(newPhoneNumber.length - 1)
        if (!(typeof Number(latestChar) !== "number" || Number.isNaN(Number(latestChar)))) {
            setPhoneNumber(newPhoneNumber) 
        }
    }

    if (phoneNumber.length >= 10) {
        addPhoneNumber(phoneNumber)
        setPhoneNumber("")
    }

    return (
        <input type="text" onChange={setNumber} value={phoneNumber} />
    )
}

export default InputField