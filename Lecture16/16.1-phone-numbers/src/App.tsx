import { useState } from 'react'
import InputField from './InputField'

const initialArray: string[] = []

const App = () => {
    const [phoneNumbers, setPhoneNumbers] = useState(initialArray.slice())

    const addPhoneNumber = (newPhoneNumber: string) => {
        setPhoneNumbers(phoneNumbers.concat(newPhoneNumber))
    }

    return (
        <>
            <h1>Phone numbers</h1>
            <InputField addPhoneNumber={addPhoneNumber}/>

            <ul>
                {phoneNumbers.map(phoneNumber => {
                    return (
                        <li>{phoneNumber}</li>
                    )
                })}
            </ul>
        </>
    )
}

export default App
