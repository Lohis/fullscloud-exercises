import { useState } from "react"
import { Contact } from "./contact"

interface ContactEditProps {
    contact: Contact
    switchToView: (viewName: string) => void
}

const ContactEdit = (props: ContactEditProps) => {
    const { contact, switchToView } = props

    const [name, setName] = useState(contact.name)
    const [email, setEmail] = useState(contact.email)
    const [phone, setPhone] = useState(contact.phone)
    const [address, setAddress] = useState(contact.address)
    const [website, setWebsite] = useState(contact.website)
    const [notes, setNotes] = useState(contact.notes)

    const cancelEditing = () => {
        switchToView("")
    }

    const saveContact = () => {
        if (name !== "") {
            contact.name = name
            contact.email = email
            contact.phone = phone
            contact.address = address
            contact.website = website
            contact.notes = notes
            switchToView("")
        } else alert("Name cannot be empty")
    }

    return (
        <>
            <div>
                <h1>Edit contact</h1>
            </div>
            <div>
                <legend>Name</legend>
                <input type="txt" value={name} onChange={(e) => setName(e.target.value)}></input>
            </div>
            <div>
                <legend>Email address</legend>
                <input type="txt" value={email} onChange={(e) => setEmail(e.target.value)}></input>
            </div>
            <div>
                <legend>Phone number</legend>
                <input type="txt" value={phone} onChange={(e) => setPhone(e.target.value)}></input>
            </div>
            <div>
                <legend>Address</legend>
                <input type="txt" value={address} onChange={(e) => setAddress(e.target.value)}></input>
            </div>
            <div>
                <legend>Website</legend>
                <input type="txt" value={website} onChange={(e) => setWebsite(e.target.value)}></input>
            </div>
            <div>
                <legend>Notes</legend>
                <textarea rows="2" value={notes} onChange={(e) => setNotes(e.target.value)}></textarea>
            </div>
            <div>
                <button onClick={saveContact}>Save</button>
                <button onClick={cancelEditing}>Cancel</button>
            </div>
        </>


    )
}

export default ContactEdit