import { useState } from 'react'
import "./App.css"
import { Contact } from './contact'
import ContactList from './ContactList'
import ContactNoneSelected from './ContactNoneSelected'
import ContactView from './ContactView'
import ContactAdd from './ContactAdd'
import ContactEdit from './ContactEdit'

const defaultContact: Contact = {
    id: 0,
    name: "default",
    email: "",
    phone: "",
    address: "",
    website: "",
    notes: ""
}

const testContact1: Contact = {
    id: 1,
    name: "Leena Niemelä",
    email: "leeniem@spostitoo.sa",
    phone: "04714839577245",
    address: "Tietie 11 A 2",
    website: "duckduckgo.com",
    notes: "muista käydä Keuruulla syksyllä 2024"
}

const testContact2: Contact = {
    id: 2,
    name: "Leo Nieminen",
    email: "leoniemine@postiloo.ta",
    phone: "05024783572835",
    address: "Kotiosote 3",
    website: "asdfasdfdsf.asd",
    notes: "soita keväällä polttopuista"
}

const testContacts: Contact[] = [defaultContact, testContact1, testContact2]
let nextContactId = testContacts.length


/*
Note: making everything to flow through the parent component makes it messy pretty fast.
*/


const App = () => {
    const [contacts, setContacts] = useState(testContacts) //contacts list must at all times have a default entry at index 0
    const [view, setView] = useState("")
    const [activeContactId, setActiveContactId] = useState(0)

    //This selection method is called from tsx when rendering this component
    const selectActiveView = () => {
        let activeView = <ContactNoneSelected />
        if (view === "add") activeView = <ContactAdd addContact={addContact} switchToView={switchToView} />
        if (view === "edit") activeView = <ContactEdit contact={findContactOrDefault(activeContactId)} switchToView={switchToView} />
        if (view === "view") activeView = <ContactView contact={findContactOrDefault(activeContactId)} removeContact={removeActiveContact} switchToView={switchToView}/>
        return activeView
    }

    const addContact = (newContact: Contact) => {
        newContact.id = nextContactId++
        setContacts([...contacts, newContact])
        switchToView("")
    }

    const findContactOrDefault = (contactId: number) => {
        let existingContact = contacts.find(contact => contact.id === contactId)
        if (typeof existingContact === "undefined") existingContact = contacts[0] //this assumes that a placeholder item exists as the first list entry
        return existingContact
    }

    const showContact = (contactId: number) => {
        const existingContact = contacts.find(contact => contact.id === contactId)
        if (typeof existingContact !== "undefined") {
            setActiveContactId(contactId)
            switchToView("view")
        }
    }

    const removeActiveContact = () => {
        setContacts(contacts.filter(contact => contact.id !== activeContactId))
        setActiveContactId(0)
        switchToView("")
    }

    const switchToView = (viewName: string) => {
        setView(viewName)
    }

    return (
        <>
            <div className="container">
                <div className="column left">
                    <ContactList contacts={contacts} switchToView={switchToView} showContact={showContact}/>
                </div>
                <div className="column right">
                    {selectActiveView()}
                </div>
            </div>
        </>
    )
}

export default App
