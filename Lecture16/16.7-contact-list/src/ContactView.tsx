import { Contact } from "./contact"

interface ContactViewProps {
    contact: Contact
    removeContact: () => void
    switchToView: (viewName: string) => void
}

const ContactView = (props: ContactViewProps) => {
    const { contact, removeContact, switchToView } = props

    return (
        <>
            <h1>{contact.name}</h1>
            <div>
                <i>Phone:</i> {contact.phone}
            </div>
            <div>
                <i>Email:</i> {contact.email}
            </div>
            <div>
                <i>Address:</i> {contact.address}
            </div>
            <div>
                <i>Notes:</i> {contact.notes}
            </div>
            <div>
                <button onClick={removeContact}>Remove</button>
                <button onClick={() => switchToView("edit")}>Edit</button>
            </div>
        </>
    )
}

export default ContactView