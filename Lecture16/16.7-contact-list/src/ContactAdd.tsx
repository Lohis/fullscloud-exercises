import { useEffect, useState } from "react"
import { Contact } from "./contact"

interface ContactAddProps {
    addContact: (newContact: Contact) => void
    switchToView: (viewName: string) => void
}

const ContactAdd = (props: ContactAddProps) => {
    const { addContact, switchToView } = props

    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [phone, setPhone] = useState("")
    const [address, setAddress] = useState("")
    const [website, setWebsite] = useState("")
    const [notes, setNotes] = useState("")

    const saveContact = () => {
        if (name !== "") {
            const newContact = {
                id: 0,
                name: name,
                email: email,
                phone: phone,
                address: address,
                website: website,
                notes: notes
            }
            addContact(newContact)
        }
    }

    const cancelAdding = () => {
        switchToView("") 
    }

    const clearFields = () => {
        setName("")
        setEmail("")
        setPhone("")
        setAddress("")
        setWebsite("")
        setNotes("")
    }

    useEffect(() => {
        clearFields()
    }, [])

    return (
        <>
            <div>
                <h1>Add contact</h1>
            </div>
            <div>
                <legend>Name</legend>
                <input type="txt" value={name} onChange={(e) => setName(e.target.value)}></input>
            </div>
            <div>
                <legend>Email address</legend>
                <input type="txt" value={email} onChange={(e) => setEmail(e.target.value)}></input>
            </div>
            <div>
                <legend>Phone number</legend>
                <input type="txt" value={phone} onChange={(e) => setPhone(e.target.value)}></input>
            </div>
            <div>
                <legend>Address</legend>
                <input type="txt" value={address} onChange={(e) => setAddress(e.target.value)}></input>
            </div>
            <div>
                <legend>Website</legend>
                <input type="txt" value={website} onChange={(e) => setWebsite(e.target.value)}></input>
            </div>
            <div>
                <legend>Notes</legend>
                <textarea rows="2" value={notes} onChange={(e) => setNotes(e.target.value)}></textarea>
            </div>
            <div>
                <button onClick={saveContact}>Save</button>
                <button onClick={cancelAdding}>Cancel</button>
            </div>
            
        </>
    )
}

export default ContactAdd