import { useState } from "react"
import { Contact } from "./contact"

interface ContactListProps {
    contacts: Contact[]
    switchToView: (viewName: string) => void
    showContact: (contactId:number) => void
}

const ContactList = (props: ContactListProps) => {
    const { contacts, switchToView, showContact } = props
    
    const [searchString, setSearchString] = useState("")

    const onContactSelect = (contactId: number) => {
        showContact(contactId)
    }

    return (
        <>
            <input type="text" value={searchString} onChange={(e) => setSearchString(e.target.value)}></input>
            {contacts.map((contact) => {
                if (contact.id !== 0 && contact.name.toLowerCase().includes(searchString.toLowerCase())) {
                    return (
                        <div onClick={() => onContactSelect(contact.id)}>{contact.name}</div>
                    )
                }
            })}
            <button onClick={() => switchToView("add")}>Add contact</button>
        </>
    )
}

export default ContactList