import { useEffect, useState } from 'react'
import axios from 'axios'

/*
NOTE: App logic is implemented, and fetching the content works.
Showing the fetched image is not functioning. Most likely due to an issue with image data encoding.
If there's time left, I will investigate further.
*/

function App() {
    const [input, setInput] = useState("")
    const [content, setContent] = useState("")
    const [imageNumber, setImageNumber] = useState(1) //used just to trigger re-rendering of the App

    useEffect(() => {
        const fetchData = async () => {
            let requestUrl = "https://cataas.com/cat"
            if (input !== "") requestUrl += `/says/${input}`

            const response = await axios.get(requestUrl)
            const imageData = response.data //image is returned as binary data
            const imageType = response.headers["content-type"]

            const convertedImageData = /*makeAConversionToBase64(imageData)*/ imageData
            const newContent = `data:${imageType};base64,${convertedImageData}`

            setContent(newContent)
            setInput("")
        }
        fetchData()

    }, [imageNumber])

    const onReload = () => {
        setImageNumber(imageNumber + 1)
    }

    return (
        <>
            <h1>More cats</h1>
            <div>
                <img src={content} width="400px"></img>
            </div>
            <div>
                <input type="text" value={input} onChange={(e) => {setInput(e.target.value)}} />
                <button onClick={onReload}>Reload</button>
            </div>
        </>
    )
}

export default App
