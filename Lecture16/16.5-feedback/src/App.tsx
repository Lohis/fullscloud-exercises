import { useEffect, useState } from 'react'

const App = () => {
    const [feedbackType, setFeedbackType] = useState("")
    const [feedback, setFeedback] = useState("")
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [formCanBeSent, setFormCanBeSent] = useState(false)


    const onSubmit = (e: { preventDefault: () => void }) => {
        e.preventDefault() // this prevents the page from reloading
        // authentication code here
        sendFormData()
        clearForm()
    }

    const onReset = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        clearForm()
    }

    const clearForm = () => {
        setFeedbackType("")
        setFeedback("")
        setName("")
        setEmail("")
    }

    const sendFormData = () => {
        console.log(
            `FORM SUBMIT
            feedback type: ${feedbackType}
            name: ${name}
            email: ${email}
            feedback: ${feedback}`
        )
    }

    useEffect(() => {
        setFormCanBeSent(feedbackType !== "" && feedback !== "")
    }, [feedbackType, feedback])

    return (
        <>
            <h1>Feedback</h1>

            <form>
                <div onChange={(e) => { setFeedbackType(e.target.value) }}>
                    <div>
                        <input
                            type="radio"
                            name="feedbackType"
                            value="feedback"
                            checked={feedbackType === "feedback"}
                        />Feedback
                    </div>
                    <div>
                        <input
                            type="radio"
                            name="feedbackType"
                            value="suggestion"
                            checked={feedbackType === "suggestion"}
                        />Suggestion
                    </div>
                    <div>
                        <input
                            type="radio"
                            name="feedbackType"
                            value="question"
                            checked={feedbackType === "question"}
                        />Question
                    </div>
                </div>
                <div>
                    <textarea name="feedback" value={feedback} onChange={(e) => { setFeedback(e.target.value) }}></textarea>
                </div>
                <div>
                    <input name="name" type="text" value={name} onChange={(e) => { setName(e.target.value) }}></input>
                </div>
                <div>
                    <input name="email" type="text" value={email} onChange={(e) => { setEmail(e.target.value) }}></input>
                </div>
                <div>
                    <button onClick={onSubmit} disabled={!formCanBeSent}>Send</button>
                    <button onClick={onReset}>Reset</button>
                </div>
            </form>
        </>
    )
}

export default App
