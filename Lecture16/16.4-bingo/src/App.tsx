import { useState } from 'react'

const maxNum = 75

const App = () => {
    const [bingoNumbers, setBingoNumbers] = useState([])

    const generateRandomInt = (min: number, max: number) => {
        const num = Math.floor(Math.random() * (max - min)) + min
        return Math.round(num)
    }

    const generateNewBingoNumber = () => {
        let newBingoNumber = 0
        let generatedNum = 0

        while (newBingoNumber === 0 && bingoNumbers.length < maxNum) {
            generatedNum = generateRandomInt(1, maxNum + 1)
            const existingMatch = bingoNumbers.find(existingNum => existingNum == generatedNum)
            if (typeof existingMatch === "undefined") {
                newBingoNumber = generatedNum
            }
        }

        if (newBingoNumber !== 0) {
            setBingoNumbers(bingoNumbers.concat(newBingoNumber))
        }
    }

    return (
        <>
            <h1>Keskiviikkobingo</h1>
            {bingoNumbers.map((bingoNumber) => {
                return (
                    <div>{bingoNumber}</div>
                )
            })}
            <button onClick={generateNewBingoNumber}>+</button>
        </>
    )
}

export default App
