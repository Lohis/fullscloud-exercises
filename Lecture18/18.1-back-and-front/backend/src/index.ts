import express, { Request, Response } from "express"

const server = express()
const port = 3001

const languages = ["Javascript","Python","Go","Java","Kotlin","Scala","C","Swift","TypeScript","Ruby"]

server.get("/languages", async (_req: Request, res: Response) => {
    res.send(languages)
})

server.listen(port, () => console.log("Server listening at port", port))