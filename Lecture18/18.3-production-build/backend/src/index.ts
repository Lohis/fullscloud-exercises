import express, { Request, Response } from "express"
import languagesDao from "./db/languagesDao"
import "dotenv/config"

const server = express()
const port = 3001

server.use("/", express.static("./dist/client"))

server.get("/languages", async (_req: Request, res: Response) => {
    const languages = await languagesDao.getLanguages()
    console.log("Request: GET /languages\nResponse data:\n", languages)
    res.send(languages)
})

server.listen(port, () => console.log("Server listening at port", port))