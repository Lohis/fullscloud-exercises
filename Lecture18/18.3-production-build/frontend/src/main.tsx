import React from 'react'
import ReactDOM from 'react-dom/client'
import Languages from './Languages.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <Languages />
    </React.StrictMode>,
)
