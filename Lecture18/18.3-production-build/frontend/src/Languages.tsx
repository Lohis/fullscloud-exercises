import { useEffect, useState } from 'react'
import axios from 'axios'

const placeholderLanguages: LanguageEntry[] = []

interface LanguageEntry {
    id: number
    name: string
}

function App() {
    const [languages, setLanguages] = useState(placeholderLanguages)

    const initialize = async () => {
        /*
        //fetch alternative
        const languages = await fetch("/languages")
            .then(response => response.json())
        setLanguages(languages)
        */

        await axios.get("/languages")
            .then((response) => {
                setLanguages(response.data)
            }
        ) 
    }

    useEffect(() => {
       initialize()
    }, [])

    return (
        <div>
            <h1>Languages</h1>
            <ul>
            {languages.map(language => {
                return (
                    <li>{language.name}</li>
                )
            })}
            </ul>
        </div>
    )
}

export default App
