import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import BlogApp from './BlogApp'
import BlogPostPage, {loader as postLoader} from "./BlogPostPage"
import ErrorPage from './ErrorPage'

const router = createBrowserRouter([
    {
        path: "/",
        element: <BlogApp />,
        errorElement: <ErrorPage />,
        children: [
            {
                path: "/posts/:id",
                element: <BlogPostPage />,
                loader: postLoader
            }
        ]
    },
])

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>,
)
