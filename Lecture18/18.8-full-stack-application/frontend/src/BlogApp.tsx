import { useEffect, useState } from 'react'
import { Link, Outlet } from "react-router-dom"
import axios from 'axios'
import BlogPost from './blogPost'
import NewPostWriter from "./NewPostWriter"

const placeholderBlogPost: BlogPost = { id: 0, title: "Placeholder post", body: "This is a placeholder blog post" }

const BlogApp = () => {
    const [posts, setPosts] = useState([placeholderBlogPost])
    const [activePostId, setActivePostId] = useState(0)

    const initialize = async () => {
        console.log("Initialize front")
        const fetchedPosts = (await axios.get("/api/posts")).data
        setPosts(fetchedPosts)
    }

    useEffect(() => {
        initialize()
    }, [])

    const addNewPost = async (postTitle: string, postBody: string) => {
        console.log(`New post ready for sending\nTitle: ${postTitle} Body: ${postBody}`)
        await axios.post("/api/posts", { title: postTitle, body: postBody })
        initialize() //Initialize to trigger fetching posts from backend, including the new addition
    }

    return (
        <div>
            <div>
                <h1>Blog posts</h1>
                <nav>
                    {posts.map(post => {
                        if (post.id === activePostId) {
                            return (
                                <Link style={{ backgroundColor: "yellow" }} onClick={() => { setActivePostId(post.id) }} to={`posts/${post.id}`}>{post.title} </Link>
                            )
                        } else {
                            return (
                                <Link onClick={() => { setActivePostId(post.id) }} to={`posts/${post.id}`}>{post.title} </Link>
                            )
                        }
                    })}
                </nav>

                <Outlet />

            </div>

            <NewPostWriter addNewPost={addNewPost} />

        </div>
    )
}

export default BlogApp
