export default interface BlogPost {
    id: number
    title: string
    body?: string
}

export const placeholderBlogPost: BlogPost = {
    id: 0,
    title: "Placeholder",
    body: "Placeholder blog post"
}
