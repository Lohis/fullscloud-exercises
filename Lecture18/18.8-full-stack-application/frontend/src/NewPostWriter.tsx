import { useState } from "react"

interface NewPostWriterProps {
    addNewPost: (postTitle: string, postBody:string) => void
}

const NewPostWriter = (props: NewPostWriterProps) => {
    const { addNewPost } = props

    const [title, setTitle] = useState("")
    const [body, setBody] = useState("")

    const onAddPost = () => {
        if (title !== "" && body !== "") {
           addNewPost(title, body)
           setTitle("")
           setBody("")
        } else {
            alert("Check that the new post has a title and text body")
        }
    }

    return (
        <div>
            <h1>Write new post</h1>
            <div>
                <legend>Title</legend>
                <input type="text" value={title} onChange={(e) => setTitle(e.target.value)}></input>
            </div>
            <div>
                <legend>Blog text</legend>
                <textarea rows={8} value={body} onChange={(e) => setBody(e.target.value)}></textarea>
            </div>
            <div>
                <button onClick={onAddPost}>Add post</button>
            </div>
        </div>
    )
}

export default NewPostWriter