import { useLoaderData } from "react-router-dom"
import axios from "axios"
import BlogPost from "./blogPost"

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const loader = async ({params}: any) => {
    const postId = Number(params.id)
    const post = (await axios.get(`/api/posts/${postId}`)).data
    console.log("Post:", post)
    if (post === "") throw Error("Post not found")
    return post as BlogPost
} 

const BlogPostPage = () => {
    const post = useLoaderData() as BlogPost
    
    return (
        <div>
            <h2>{post.title}</h2>
            <p>{post.body}</p>
        </div>
    )
}

export default BlogPostPage