import { useRouteError, Link, isRouteErrorResponse } from 'react-router-dom'

export default function ErrorPage() {
    const error = useRouteError() as Error
    console.error(error)

    if (!isRouteErrorResponse(error)) {     

        if (error.message === "Post not found") {
            return (
                <div className='ErrorPage'>
                    <h1>404 - Not Found</h1>
                    <p>The requested post is not found</p>
                    <Link to={'/'}>Main Page</Link>
                </div>
            )
        }

        return (
            <div className='ErrorPage'>
                <h1>Unknown error</h1>
                <p>No further info available</p>
                <Link to={'/'}>Main Page</Link>
            </div>
        )
    }

    if (error.status === 404) {
        return (
            <div className='ErrorPage'>
                <h1>404 - Not Found</h1>
                <p>The requested item is not here</p>
                <Link to={'/'}>Main Page</Link>
            </div>
        )
    }

    return (
        <div className='ErrorPage'>
            <h1>Unexpected error</h1>
            <p>({error.status}) {error.statusText}</p>
            {error.data?.message && <p>{error.data.message}</p>}
            <Link to={'/'}>Main Page</Link>
        </div>
    )
}