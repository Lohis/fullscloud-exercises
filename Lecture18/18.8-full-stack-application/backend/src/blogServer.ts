import express, { Request, Response, NextFunction } from "express"
import blogPostsDao from "./db/blogPostsDao"

const server = express()
server.use("/", express.static("./dist/client"))
server.use(express.json())

//Request logging middleware
server.use("/", (req: Request, _res: Response, next: NextFunction) => {
    console.log("Request received:", req.method, req.path, req.body)
    next()
})

//New blog post validation middleware
const validateNewPostHasTitleAndBody = (req: Request, res: Response, next: NextFunction) => {
    const { title, body } = req.body
    if (typeof title !== "string" || title === "") return res.sendStatus(400)
    if (typeof body !== "string" || title === "") return res.sendStatus(400)

    next()
}

server.get("/version", (_req: Request, res: Response) => {
    const { BACKEND_VERSION, FRONTEND_VERSION } = process.env
    res.send(`backend version: ${BACKEND_VERSION}\nfrontend version: ${FRONTEND_VERSION}`)
})

server.get("/api/posts", async (_req: Request, res: Response) => {
    const responseData = await blogPostsDao.getBlogPosts()
    res.send(responseData)
})

server.get("/api/posts/:id", async (req: Request, res: Response) => {
    const id = Number(req.params.id)
    const responseData = await blogPostsDao.getBlogPost(id)
    res.send(responseData[0])
})

server.post("/api/posts", validateNewPostHasTitleAndBody, async (req: Request, res: Response) => {
    const { title, body } = req.body
    console.log(`New post received\nTitle: ${title}\nBody: ${body}`)
    await blogPostsDao.addBlogPost(title, body)
    res.sendStatus(201)
})

export default server
