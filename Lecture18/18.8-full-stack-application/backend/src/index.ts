import server from "./blogServer"

const isValidEnv = (): boolean => {
    const { PORT, PG_HOST, PG_PORT, PG_USER, PG_PASSWORD, PG_DATABASE, USE_SSL, BACKEND_VERSION, FRONTEND_VERSION } = process.env
    
    if (typeof PORT !== "string" || PORT === "") return false
    if (typeof PG_HOST !== "string" || PG_HOST === "") return false
    if (typeof PG_PORT !== "string" || PG_PORT === "") return false
    if (typeof PG_USER !== "string" || PG_USER === "") return false
    if (typeof PG_PASSWORD !== "string" || PG_PASSWORD === "") return false
    if (typeof PG_DATABASE !== "string" || PG_DATABASE === "") return false
    if (typeof USE_SSL !== "string" || USE_SSL === "") return false
    if (typeof BACKEND_VERSION !== "string" || BACKEND_VERSION === "") return false
    if (typeof FRONTEND_VERSION !== "string" || FRONTEND_VERSION === "") return false
    
    return true
}

if (!isValidEnv()) {
    console.log("Error: no required environment variables found")
    process.exit(1)
}

const { PORT } = process.env
server.listen(PORT, () => console.log("Server listening at port", PORT))
