import { queries } from "./queries"
import executeQuery from "./db"

const getBlogPosts = async () => {
    const result = await executeQuery(queries.blogposts.getAll)
    return result.rows
}

const getBlogPost = async (id: number) => {
    const queryParams = [id]
    const result = await executeQuery(queries.blogposts.get, queryParams)
    return result.rows
}

const addBlogPost = async (title: string, body: string) => {
    const queryParams = [title, body]
    await executeQuery(queries.blogposts.add, queryParams)
    return
}

export default { getBlogPosts, getBlogPost, addBlogPost }