export const queries = {
    blogposts: {
        getAll: "SELECT id, title FROM blogposts;",
        get: "SELECT * FROM blogposts WHERE id = $1",
        add: "INSERT INTO blogposts (title, body) VALUES ($1, $2)"
    },
} 