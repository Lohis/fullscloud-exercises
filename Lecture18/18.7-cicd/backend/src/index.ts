import express, { Request, Response } from "express"
import languagesDao from "./db/languagesDao"

const server = express()
const { PORT } = process.env




server.use("/", express.static("./dist/client"))

server.get("/api/languages", async (_req: Request, res: Response) => {
    console.log("GET /api/languages")
    const languages = await languagesDao.getLanguages()
    console.log("Response data:\n", languages)
    res.send(languages)
})

server.listen(PORT, () => console.log("Server listening at port", PORT))