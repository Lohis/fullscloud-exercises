Languages App - A full-stack application

Unfortunately, at the moment the deployed application
does not work fully.

Good: The frontend gets served from Azure,
and the database running at Azure can be accessed from locally running app container.

Bad: The API endpoint is not working after deployment to Azure,
it returns status code 502 Bad Gateway. According to Log stream at Azure, the container crashes. (Connection fails between Web Service and Azure Database?)
-EDIT: the reason found - there was an error in Gitlab ci/cd environment variables for database access credentials
