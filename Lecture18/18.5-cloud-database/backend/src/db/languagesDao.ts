import { queries } from "./queries"
import executeQuery from "./db"

const getLanguages = async () => {
    const result = await executeQuery(queries.languages.getAll)
    return result.rows
}

export default { getLanguages }