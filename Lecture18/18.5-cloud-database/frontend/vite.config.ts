import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      "/api/languages": {
        target: "/api/languages",
        changeOrigin: true,
        secure: false
      }
    }
  }
})
