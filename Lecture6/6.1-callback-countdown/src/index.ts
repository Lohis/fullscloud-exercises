/*
console.log("3 => Wait 1 second")
setTimeout(() => console.log("2 => Wait another second"), 1000)
setTimeout(() => console.log("1 => Wait the last 1 second..."), 2000)
setTimeout(() => console.log("GO"), 3000)
*/

console.log("3 => Wait 1 second")
setTimeout(() => {
    console.log("2 => Wait another second")
    setTimeout(() => {
        console.log("1 => Wait the last 1 second...")
        setTimeout(() => console.log("GO"), 1000)
    }, 1000)
}, 1000)
