
//populate an array for testing purposes
const arrSize = 500
const arr: number[] = []
for (let i = 0; i < arrSize; i++) {
    arr.push(Math.random() * 10000)
}

const findLargest = (arr: number[]) => {
    if (arr.length === 0) return null //empty array is a corner case that needs to be handled somehow
    
    let largest = arr[0]
    arr.forEach((item) => {
        if (item > largest) largest = item
    })

    return largest
}

const findSecondLargest = (arr: number[]) => {
    if (arr.length === 0) return null

    const largest = findLargest(arr)
    let secondLargest = arr[0]
    arr.forEach((item) => {
        if (item > secondLargest && item !== largest) secondLargest = item
    })

    return secondLargest
}

console.log(`Largest: ${findLargest(arr)}`)
console.log(`Second largest: ${findSecondLargest(arr)}`)
