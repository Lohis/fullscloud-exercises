import axios from "axios"

const todosUrl = "https://jsonplaceholder.typicode.com/todos/"
const usersUrl = "https://jsonplaceholder.typicode.com/users/"

interface TodoItem {
    userId: number
    id: number
    title: string
    completed: boolean
    user?: User
}

interface User {
    id?: number
    name: string
    username: string
    email: string
}

const getAndCombineData = async (): Promise<TodoItem[]> => {
    const todosResponse = await axios.get(todosUrl)
    const todos: TodoItem[] = todosResponse.data
    
    const usersResponse = await axios.get(usersUrl)
    let users: User[] = usersResponse.data

    //Remove unnecessary properties from users
    users = users.map((user) => {
        const modifiedUser = new Object as User
        modifiedUser.id = user.id
        modifiedUser.name = user.name
        modifiedUser.username = user.username
        modifiedUser.email = user.email
        return modifiedUser
    })

    //Add users into todo properties
    todos.forEach((todo) => {
        const matchingUser = users.find((user) => {
            if (user.id === todo.userId) return user
        })

        //Remove userId property from user
        //Note: in order to remove just one property, "leaving it out" like this isn't the best approach - how to just delete one property instead?
        if (matchingUser) {
            const modifiedUser = new Object as User
            modifiedUser.name = matchingUser.name
            modifiedUser.username = matchingUser.username
            modifiedUser.email = matchingUser.email
            todo.user = modifiedUser
        }
    })

    return todos
}

getAndCombineData().then((result) => console.log(result))

