const enableDebugPrint = false

//F1 1992 driver lineup at season start
const driverNames = [
    "Senna", "Berger",
    "Grouillard", "Cesaris",
    "Mansell", "Patrese",
    "van de Poele", "Amati",
    "Alboreto", "Suzuki",
    "Häkkinen", "Herbert",
    "Chiesa", "Tarquini",
    "Wendlinger", "Belmondo",
    "Schumacher", "Brundle",
    "Lehto", "Martini",
    "Fittipaldi", "Morbidelli",
    "Boutsen", "Comas",
    "Alesi", "Capelli",
    "Gachot", "Katayama",
    "Modena", "Gugelmin",
    "Caffi", "Bertaggia"
]

interface Driver {
    name: string
    isRunning: boolean
    totalTime: number
    bestLap: number
}

const debugLog = (msg: string) => {
    if (enableDebugPrint) console.log(msg)
}

const generateRandomNumber = (min: number, max: number) => Math.random() * (max-min) + min

const getLapTime = () => generateRandomNumber(20, 25)

const getRunningStatus = () => Math.random() > 0.03

const raceLap = (): Promise<number> => {
    return new Promise((resolve, reject) => {
        return getRunningStatus() ? resolve(getLapTime()) : reject()
    })
}

const initializeDrivers = (driverNames: string[]): Driver[] => {
    let drivers: Driver[] = []
    drivers = driverNames.map((driverName) => {
        const newDriver = new Object as Driver
        newDriver.name = driverName
        newDriver.isRunning = true
        newDriver.totalTime = 0
        newDriver.bestLap = Number.MAX_VALUE
        return newDriver
    })
    return drivers
}

const determineWinner = (drivers: Driver[]): Driver | null => {
    let winner: Driver | null = null
    drivers.forEach((driver) => {
        if (driver.isRunning) {
            if (!winner) winner = driver
            else if (driver.totalTime < winner.totalTime) {
                winner = driver
            }
        }
    })
    return winner
}

const race = async (driverNames: string[], laps: number): Promise<string> => {
    const drivers = initializeDrivers(driverNames)
    for (let i = 0; i < laps; i++) {
        debugLog(`It's lap number ${i + 1}`)
        for (let j = 0; j < drivers.length; j++) {
            const driver = drivers[j]
            if (driver.isRunning) {
                await raceLap()
                    .then((lapTime) => {
                        driver.totalTime += lapTime
                        debugLog(`${driver.name} finishes a lap in ${lapTime}, total time: ${driver.totalTime}`)
                        if (lapTime < driver.bestLap) {
                            driver.bestLap = lapTime
                        }
                    })
                    .catch(() => {
                        driver.isRunning = false
                        debugLog(`${driver.name} is out of the race!`)
                    })
            }
        }
    }
    debugLog("The race is finished!")

    const winner = determineWinner(drivers)
    let winnerStats = ""
    if (winner) {
        winnerStats = `█ █ █ █ █ █ █ █ █ █ █\n █ █ █ █ █ █ █ █ █ █ █\nThe race has ended!\nThe winner is ${winner.name}\nTotal time: ${winner.totalTime} seconds\nBest lap: ${winner.bestLap} seconds`
    } else winnerStats = "No one won the race"
    return winnerStats
}

race(driverNames, 10).then((winnerStats) => {
    console.log(winnerStats)
})