const students = [
    { name: "Markku", score: 99 },
    { name: "Karoliina", score: 58 },
    { name: "Susanna", score: 69 },
    { name: "Benjamin", score: 77 },
    { name: "Isak", score: 49 },
    { name: "Liisa", score: 89 },
]

const gradeLowerLimits = [1, 40, 60, 80, 95]

interface Student {
    name: string
    score: number
    grade?: number
}

const studentArr: Student[] = students

const findStudentWithHighestOrLowestScore = (studentArr: Student[], searchForHighest: boolean): Student => {
    let largestValue = searchForHighest ? Number.MIN_VALUE : Number.MAX_VALUE
    let largestIndex = 0
    for (let i = 0; i < studentArr.length; i++) {
        if (searchForHighest) {
            if (studentArr[i].score > largestValue) {
                largestValue = studentArr[i].score
                largestIndex = i
            }
        } else {
            if (studentArr[i].score < largestValue) {
                largestValue = studentArr[i].score
                largestIndex = i
            }
        }
    }
    return studentArr[largestIndex]
}

const findStudentsWithHighestAndLowestScore = (studentArr: Student[]): Student[] => {
    const studentsWithHighestAndLowestScore: Student[] = []
    studentsWithHighestAndLowestScore.push(findStudentWithHighestOrLowestScore(studentArr, true))
    studentsWithHighestAndLowestScore.push(findStudentWithHighestOrLowestScore(studentArr, false))
    return studentsWithHighestAndLowestScore
}


const calculateAverageScore = (studentArr: Student[]) => {
    const totalScore = studentArr.reduce((acc, cur) => {
        return acc += cur.score
    }, 0)
    return totalScore / studentArr.length
}


const filterStudentsWithAboveAverageScore = (studentArr: Student[]): Student[] => {
    const averageScore = calculateAverageScore(studentArr)
    const studentsWithAboveAverageScore = studentArr.filter((item) => {
        if (item.score > averageScore) return item
    })
    return studentsWithAboveAverageScore
}

console.log("----------------------------------------")
console.log("Students with highest and lowest score:")
const highestAndLowest = findStudentsWithHighestAndLowestScore(studentArr)
highestAndLowest.forEach((item) => {
    console.log(`Name: ${item.name} | Score: ${item.score}`)
})
console.log("----------------------------------------")
console.log("Average score:")
console.log(calculateAverageScore(studentArr))
console.log("----------------------------------------")
console.log("Students with above average score:")
console.log(filterStudentsWithAboveAverageScore(studentArr))
console.log("----------------------------------------")

const gradeStudents = (studentArr: Student[]): void => {
    studentArr.forEach((item) => {
        const score = item.score
        let grade = 0
        
        if (score > gradeLowerLimits[4]) grade = 5
        else if (score > gradeLowerLimits[3]) grade = 4
        else if (score > gradeLowerLimits[2]) grade = 3
        else if (score > gradeLowerLimits[1]) grade = 2
        else if (score > gradeLowerLimits[0]) grade = 1

        item.grade = grade
    })
}

gradeStudents(studentArr)

console.log("Graded students:")
console.log(studentArr)