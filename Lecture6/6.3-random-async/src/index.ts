const getValue = function (): Promise<number> {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}

const getValuesAsync = async () => {
    const value1 = await getValue()
    const value2 = await getValue()
    console.log(value1, value2)
}

const getValuesPromise = () => {
    getValue().then((value1) => {
        getValue().then((value2) => {
            console.log(value1, value2)
        })
    })
}

getValuesAsync()
getValuesPromise()


