import axios from "axios"

interface Movie {
    Title: string
    Year: number
}

const getMovie = async (name: string, year?: number) => {
    const movie: Movie[] = (await axios.get(`https://omdbapi.com/?apikey=c3a0092f&s=${name}&y=${year}`)).data.Search
    movie.forEach((m) => {
        console.log(`Title: ${m.Title} | Year: ${m.Year}`)
    })
}

getMovie("Matrix", 1999)
