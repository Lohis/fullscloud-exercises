
const delayPromise = function(message: string, delay: number): Promise<string> {
    return new Promise((resolve) => {
        setTimeout(() => resolve(message), delay)
    })
}

console.log("Wait 1 second")
delayPromise("Wait another second", 1000).then(result => console.log(result))
delayPromise("Wait the last 1 second...", 2000).then(result => console.log(result))
delayPromise("GO", 3000).then(result => console.log(result))
